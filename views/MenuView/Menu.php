 

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle">Crear Menu</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="hidden" name="id" class="inputform" id="idmenus" required autocomplete="off">
          <input type="text" name="nombre" class="inputform" id="nombre" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre del menú</label>
        </div>
        <div class="grupo">
          <input type="text" name="url" class="inputform" id="url" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">URL</label>
        </div> 
        <div class="grupo">
          <select class="inputform" name="jerarquia" id="ComboJerar" required>
            <option></option>
            <option value="0">Menú</option>
            <option value="1">Sub-menú</option>
          </select><span class="barra"></span>
          <label class="labelsform">Seleccionar</label>
        </div>
        <div class="grupo" id="divMenusCombo" style="display: none;">
          <select class="inputform" name="padre" id="MenusCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Seleccionar</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closes" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-success">Guardar</button>
        <button type="button" id="GuardarEdit" style="display: none" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

  <!--Tablas-->
  <div class="card shadow mb-4 mt-4">
    <div class="card-header">
      <h6 class="m-0 font-weight-bold text-primary">Menus del sistema</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="TablaMenu" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre del Menu</th>
              <th scope="col">Url Menu</th>
              <th scope="col">Jerarquia</th>
              <th scope="col">Menu Padre</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="body_Menu">
                 
          </tbody>
        </table>
      </div>
    </div>
  </div>


<script type="text/javascript">

  //Cargar datos al iniciar la pagina
  $(document).ready(function () {

    var url2 = "index.php";

    //Ajax con datatable
        $('#TablaMenu').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "menu",
              "m": "listar",
              "p" : 0
            },
           /* "dataSrc": function (json) {
               
              //alertify.success('Success message');
              return json.data;
            },*/
          },
          "columns":[
            {"data":"IdMenu"},
            {"data":"MenusNombre"},
            {"data":"MenusUrl"},
            {"data":"MenusJerarquia"},
            {"data":"Padre"},
            {"data":"acciones"} 
          ],
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          AbrirModals();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ]
        });

        Datos("listarcombo",0, "MenusCombo");
  });



  //Visibilidad del combo
    $("#ComboJerar").change(function () {
      var x = $(this).val();
      if(x == 1){
        document.getElementById("divMenusCombo").style.display = "block";
      }else{
        $("#MenusCombo").val("");
        document.getElementById("divMenusCombo").style.display = "none";
      }
    });

    function AbrirModals() {
      $("#nombre").val("");
      $("#url").val("");
      $("#ComboJerar").val("");
      $("#MenusCombo").val("");
      $('#exampleModalCenter').modal('show');
      document.getElementById("GuardarNew").style.display = "block";
      document.getElementById("GuardarEdit").style.display = "none";
      document.getElementById("divMenusCombo").style.display = "none";
    }


  $(".closes").click(function () {
    $('#exampleModalCenter').modal('hide');
  });

    //Crear menu
    $("#GuardarNew").click(function () {
      var nombre = $("#nombre").val();
      var urlcampo = $("#url").val();
      var combojerar = $("#ComboJerar").val();
      var Menuscombonew = $("#MenusCombo").val();  
      var datos = [nombre,urlcampo,combojerar,Menuscombonew];
      Datos("createMenu",datos,1);
    });

    //Editar Menu
    $("#GuardarEdit").click(function () {

      var id = $("#idmenus").val();
      var nombre = $("#nombre").val();
      var urlcampo = $("#url").val();
      var combojerar = $("#ComboJerar").val();
      var Menuscombonew = $("#MenusCombo").val();  
      var datos = [id,nombre,urlcampo,combojerar,Menuscombonew];
      Datos("EditarMenus",datos,1);
    });


    //Se camtura el ID y se llama la funcion Datos
    //Se envia el metodo del controller y se envia el id
    function Eliminar(id) {
      Datos("eliminarMenu", id,1);
    }

    function Modificar(id) {
      $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": "ConsultaEdit",
            "p" : id
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            $("#idmenus").val(prueba["Id"]);
            $("#nombre").val(prueba["Nombre"]);
            $("#url").val(prueba["Url"]);
            $("#ComboJerar").val(prueba["jerarquia"]);

            if(prueba["jerarquia"] == 0){
              $("#MenusCombo").val(0); 
              document.getElementById("divMenusCombo").style.display = "none";
            }else{
              document.getElementById("divMenusCombo").style.display = "block";
              $("#MenusCombo").val(prueba["padre"]); 
            }



            document.getElementById("GuardarNew").style.display = "none";
            document.getElementById("GuardarEdit").style.display = "block";

            $('#exampleModalCenter').modal('show');
         }
      });
    }

    function Datos(metodo, parametros, combo) {
      //Ajax normal
      //cargar los selectores
      var metod = metodo;
      var params = parametros;
      var comb = combo;
      var menuruta = $("#menustext").val();

      //Envio de informacion al controller
      if(comb > 0){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": metod,
            "p" : params 
          },
          success: function(res) { 
           if(res != 0){
              $('#exampleModalCenter').modal('hide');
              alertify.success('Datos guardados con exito');
              setTimeout(function(){redirect(menuruta); }, 1000);
            }else{
              alertify.error("No se pudo guardar los datos!!");
            }
            
          }
        });
        //Cargar combo
      }else if(comb != null || comb != ""){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           $("#"+comb+"").html(res);
          }
        });
        return;
      }   
    }

</script>