

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle1">Crear Materias</h5>
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle2" style="display: none">Modificar Materias</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="hidden" name="Id" class="inputform" id="id" required autocomplete="off">
          <input type="text" name="Codigo" class="inputform" id="Codigo" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Codigo</label>
        </div>
        <div class="grupo">
          <input type="text" name="Nombre" class="inputform" id="Nombre" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre</label>
        </div>
        <div class="grupo">
          <input type="number" name="Semestre" class="inputform" id="Semestre" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Semestre</label>
        </div>
        <div class="grupo">
          <input type="number" name="Intencidad" class="inputform" id="Intencidad" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Intencidad Horaria</label>
        </div>
        <div class="grupo">
          <textarea class="inputform" id="Descripcion"></textarea>
          <span class="barra"></span>
          <label for="" class="labelsform">Descripción</label>
        </div> 
        <div class="grupo">
          <select class="inputform" name="padre" id="EstadoCombo" required>
          </select><span class="barra"></span>
          <label class="labelsform">Estado</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="TipoCombo" required>
          </select><span class="barra"></span>
          <label class="labelsform">Tipo de Materia</label>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn closes btn-danger" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-primary">Guardar</button>
        <button type="button" id="GuardarEdit" onclick="GuardarCambios()" style="display: none" class="btn btn-success">Modificar</button>
      </div>
    </div>
  </div>
</div>

<!--Tablas-->
<div class="card shadow mb-4 mt-4">
    <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">Materias</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="TablaMenu" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Codigo</th>
                <th scope="col">Nombre</th>
                <th scope="col">Semestre</th>
                <th scope="col">Intencidad Horaria</th>
                <th scope="col">Descripción</th>
                <th scope="col">Estado</th>
                <th scope="col">Tipo de Materia</th>
                <th>Acción</th>
            </tr>
            </thead>
            <tbody id="body_Menu">
                    
            </tbody>
        </table>
        </div>
    </div>
</div>


<script type="text/javascript">
  $(document).ready(function () {
        var url2 = "index.php";
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "materias",
            "m": "CargarMaterias",
            "p" : 0
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            for(var i =0; i< prueba.length;i++){
               $("#selectPeriodo").append("<option value='"+prueba[i]['PeriodoId']+"'>"+prueba[i]["NombrePeriodo"]+"</option>"); 
               
            }            
          }
        });
        CargarCombo();
        CargarTabla();
    });

  var tabla;

  $("#buscar").click(function () {
    tabla.destroy();
    CargarTabla();
  });

  function CargarTabla() {
    var peri = $("#selectPeriodo").val();

    if(peri>=0){

    }else{
      peri = 1;
    }

    tabla = $('#TablaMenu').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "materias",
              "m": "listar",
              "p" : peri
            },
            /*"dataSrc": function (json) {
               
              //alertify.success('Success message');
              console.log(json);
            },*/
          },
          "columns":[
            {"data":"MateriasId"},
            {"data":"MateriasCodigo"},
            {"data":"MateriasNombre"},
            {"data":"MateriasSemestre"},
            {"data":"MateriasIntencidad_H"},
            {"data":"MateriasDescripcion"},
            {"data":"EstadoNombre"},
            {"data":"TipoMateriaNombre"},
            {"data":"acciones"},
          ],
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          PopNew();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ] 
        });
  }

  function PopNew() {
    document.getElementById("GuardarNew").style.display = "block";
    document.getElementById("GuardarEdit").style.display = "none";

    document.getElementById("exampleModalCenterTitle1").style.display = "block";
    document.getElementById("exampleModalCenterTitle2").style.display = "none";
    AbrirModal();
  };

  $("#GuardarNew").click(function () {
    ObtenerInfo();
    CrearNew();
  });

  function AbrirModal() {
    $("#Codigo").val("");
    $("#Nombre").val("");
    $("#Semestre").val("");
    $("#Intencidad").val("");
    $("#Descripcion").val("");
    $("#EstadoCombo").val("");
    $("#TipoCombo").val("");
    $('#exampleModalCenter').modal('show');
  }

  var datos;

  function ObtenerInfo() {
    var codigo = $("#Codigo").val();
    var nombre = $("#Nombre").val();
    var semestre = $("#Semestre").val();
    var intencidad = $("#Intencidad").val();
    var descripcion = $("#Descripcion").val();
    var estado = $("#EstadoCombo").val();
    var tipoMa = $("#TipoCombo").val();
    datos = [codigo,nombre,semestre,intencidad,descripcion,estado,tipoMa];
  }

  $(".closes").click(function () {
    $('#exampleModalCenter').modal('hide');
  });

  function CrearNew() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "materias",
          "m": "CrearNew",
          "p" : datos
        },
        success: function(res) { 
          var respuesta = JSON.parse(res);
          if(respuesta){
            alertify.success('¡Datos Guardados con exito!');
            tabla.destroy();
            CargarTabla();
            $('#exampleModalCenter').modal('hide');
          }else{
            alertify.error('¡Error al guardar la información!');
          }

        }
    });
    datos = [];
  }


  function CargarCombo() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "materias",
          "m": "listarCombos",
          "p" : 0
        },
        success: function(res) { 
        var respuesta = JSON.parse(res);
        $("#EstadoCombo").empty();
        $("#TipoCombo").empty();

        $("#EstadoCombo").append("<option selected>Seleccionar...</option>");
        $("#TipoCombo").append("<option selected>Seleccionar...</option>");

        for (var i = 0; i < respuesta["Estados"].length; i++) {
          $Estados = respuesta["Estados"][i];
          $("#EstadoCombo").append("<option value='"+$Estados["EstadoId"]+"'>"+$Estados["EstadoNombre"]+"</option>");
        }
        for (var e = 0; e < respuesta["Materias"].length; e++) {
          $Materias = respuesta["Materias"][e];
          $("#TipoCombo").append("<option value='"+$Materias["TipoMateriaId"]+"'>"+$Materias["TipoMateriaNombre"]+"</option>");
        }
      }
      });
  }

  function Modificar(id) {
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "materias",
        "m": "BuscarPro",
        "p" : id
      },
      success: function(res) { 
        var prueba = JSON.parse(res);

        $("#Codigo").val(prueba[0]["MateriasCodigo"]);
        $("#Nombre").val(prueba[0]["MateriasNombre"]);
        $("#Semestre").val(prueba[0]["MateriasSemestre"]);
        $("#Intencidad").val(prueba[0]["MateriasIntencidad_H"]);
        $("#Descripcion").val(prueba[0]["MateriasDescripcion"]);
        $("#EstadoCombo").val(prueba[0]["EstadoId"]);
        $("#TipoCombo").val(prueba[0]["TipoMateriaId"]);
        $("#id").val(prueba[0]["MateriasId"]);

        document.getElementById("GuardarNew").style.display = "none";
        document.getElementById("GuardarEdit").style.display = "block";

        document.getElementById("exampleModalCenterTitle1").style.display = "none";
        document.getElementById("exampleModalCenterTitle2").style.display = "block";

        

        $('#exampleModalCenter').modal('show');
      }
    });
  }

  function Eliminar(id) {
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "materias",
        "m": "Eliminar",
        "p" : id
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if(respuesta == true){
          alertify.success('¡Datos Eliminados con exito!');
          tabla.destroy();
          CargarTabla();
        }else{
          //alertify.error(respuesta);
          alertify.error("La materia no se puede eliminar por que esta asociado a un programa");
        }
      }
    });
  }

  function GuardarCambios() {
    ObtenerInfo();
    var idm = $("#id").val();
    modi = [datos,idm];
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "materias",
          "m": "GuardarCambios",
          "p" : modi
        },
        success: function(res) { 
          var respuesta = JSON.parse(res);
          if(respuesta){
            alertify.success('¡Datos Guardados con exito!');
            tabla.destroy();
            CargarTabla();
            $('#exampleModalCenter').modal('hide');
          }else{
            alertify.error('¡Error al guardar la información!');
          }

        }
    });
    datos = [];
    modi = [];  
  }

</script>
