 

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle">Crear Menu</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="hidden" name="rolId" class="inputform" id="rolId" required autocomplete="off">
          <input type="text" name="nombreRol" class="inputform" id="nombreRol" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre del rol</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="estado" id="estado" required>
            <option>- Seleccione -</option>
            <option value="0">Inactivo</option>
            <option value="1">Activo</option>
          </select><span class="barra"></span>
          <label class="labelsform">Seleccionar</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger closes" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-success">Guardar</button>
        <button type="button" id="GuardarEdit" style="display: none" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

  <!--Tablas-->
  <div class="card shadow mb-4 mt-4">
    <div class="card-header">
      <h6 class="m-0 font-weight-bold text-primary">Roles del sistema</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="TablaRol" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Rol</th>
              <th scope="col">Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="body_Menu">
                 
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <!-- Modal Gestion-->
<div class="modal fade" id="FormGest" tabindex="-1" role="dialog" aria-labelledby="FormGestTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="FormGestTitle">Gestionar Permisos del Rol</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="$('#FormGest').modal('hide');" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="formGestbody">
        
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

  //Cargar datos al iniciar la pagina
  $(document).ready(function () {

    var url2 = "index.php";

    //Ajax con datatable
        $('#TablaRol').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "rols",
              "m": "listar",
              "p" : 0
            },
          },
          "columns":[
            {"data":"RolId"},
            {"data":"RolNombre"},
            {"data":"RolEstado"},
            {"data":"acciones"}
          ],
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Agregar',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          PopNew();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ] 
        });
  });


    //Abrir modal
   function PopNew() {
      $("#nombreRol").val("");
      $("#estado").val("");
      $('#exampleModalCenter').modal('show');
      document.getElementById("GuardarNew").style.display = "block";
      document.getElementById("GuardarEdit").style.display = "none";
    };

    $(".closes").click(function () {
    $('#exampleModalCenter').modal('hide');
  });

    //Crear menu
    $("#GuardarNew").click(function () {
      var nombre = $("#nombreRol").val();
      var estado = $("#estado").val(); 
      var datos = [nombre,estado];
      Datos("createRol",datos,1);
    });

    //Editar Menu
    $("#GuardarEdit").click(function () {
      var id = $("#rolId").val();
      var nombre = $("#nombreRol").val();
      var estado = $("#estado").val(); 
      var datos = [id,nombre,estado];
      Datos("editRol",datos,1);
    });

    //Se camtura el ID y se llama la funcion Datos
    //Se envia el metodo del controller y se envia el id
    function Eliminar(id) {
      Datos("deleteRol", id,1);
    }

    function Modificar(id) {
      $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "rols",
            "m": "ConsultaRol",
            "p" : id
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            $("#rolId").val(prueba["RolId"]);
            $("#nombreRol").val(prueba["RolNombre"]);
            $("#estado").val(prueba["RolEstado"]);
            
            document.getElementById("GuardarNew").style.display = "none";
            document.getElementById("GuardarEdit").style.display = "block";

            $('#exampleModalCenter').modal('show');
         }
      });
    }

    function Datos(metodo, parametros, combo) {
      //Ajax normal
      //cargar los selectores
      var metod = metodo;
      var params = parametros;
      var comb = combo;
      var menuruta = $("#menustext").val();

      //Envio de informacion al controller
      if(comb > 0){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "rols",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           if(res != 0){
              alertify.success('Datos guardados con exito');
              $('#exampleModalCenter').modal('hide');
              setTimeout(function(){redirect(menuruta); }, 1000);
            }else{
              alertify.error("No se pudo guardar los datos!!");
            }
            
          }
        });
        //Cargar combo
      }else if(comb != null || comb != ""){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           $("#"+comb+"").html(res);
          }
        });
        return;
      }   
    }

    var idGes = 0;
    function VisualizarGestion(id) {
       idGes = id;
      $.ajax({
          url: urr+"views/Rols/ModalGes.php",
          success: function(data) { 
            $("#formGestbody").html(data);
            $('#FormGest').modal('show');
          }
        });
    }
</script>
