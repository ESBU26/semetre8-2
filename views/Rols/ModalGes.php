<p>
  <div class="accordion" id="accordionExample">
  <div class="accordion-item" id="Gestion">
    
  </div>
</div>

</p> 

<script type="text/javascript">
	$(document).ready(function () {
		$.ajax({
	      url: "index.php",
	      type: 'post',
	      data: {
	        "c" : "menu",
	        "m": "listarRolesMenu",
	        "p" : idGes
	      },
	      success: function(res) { 
				var respuesta = JSON.parse(res);
				
				//Captura y asigancion de arrays
				var menus = respuesta[0];
				var subMenus = respuesta[1];
				var menusRol = respuesta[2];
				var rolT = respuesta[3];
				
				for (var i =0; i < menus.length; i++) {
					var titulos = menus[i]["Nombre"];
					var ides = menus[i]["Nombre"].replace(/ /g, "");
					ides += menus[i]["Nombre"].replace(/ /g, "");
					$("#Gestion").append('<h2 class="accordion-header" id="headingOne"><button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#'+ides+'cont" aria-expanded="true" aria-controls="'+ides+'cont" id="'+menus[i]["Id"]+'" value="'+menus[i]["Id"]+'">'+menus[i]["Nombre"]+'</button></h2>');
					//Foreach para discriminacion de permisos
					for (var h = 0; h < menusRol.length; h++) {	  
						if(menusRol[h]["IdMenu"] == menus[i]["Id"]){
							$("#Gestion").append('<div id="'+ides+'cont" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample"><div class="accordion-body" id="'+ides+'"><li class="list-group-item"><div class="row align-items-start"><div class="col">Visualizar</div><div class="col"><input type="checkbox" value="'+menus[i]["Id"]+'" id="'+menus[i]["Id"]+'switch'+menus[i]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" switch="bool" checked><label for="'+menus[i]["Id"]+'switch'+menus[i]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" id="switch'+menus[i]["Nombre"]+menus[i]["Id"]+'" data-on-label="Yes" data-off-label="No"></label></div></div></li></div></div>');
							h = menusRol.length;
						}else{
							if(h== menusRol.length-1){
								$("#Gestion").append('<div id="'+ides+'cont" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample"><div class="accordion-body" id="'+ides+'" ><li class="list-group-item"><div class="row align-items-start"><div class="col">Visualizar</div><div class="col"><input type="checkbox" value="'+menus[i]["Id"]+'" id="'+menus[i]["Id"]+'switch'+menus[i]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" switch="bool"/><label for="'+menus[i]["Id"]+'switch'+menus[i]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" id="switch'+menus[i]["Nombre"]+menus[i]["Id"]+'" data-on-label="Yes" data-off-label="No"></label></div></div></li></div></div>');
							}
						}       			
					}

					//Foreach para la recorrida de subMenus y generacion de lista
					for (var j = 0; j < subMenus.length; j++) {     		
						var ides2 = subMenus[j]["Nombre"];
						if (menus[i]["Id"] == subMenus[j]["padre"]) {
							//Foreach para discriminacion de permisos
							for (var h = 0; h < menusRol.length; h++) {
								if(menusRol[h]["IdMenu"] == subMenus[j]["Id"]){
									$("#"+ides).append('<li class="list-group-item"><div class="row align-items-start"><div class="col">'+subMenus[j]["Nombre"]+'</div><div class="col"><input type="checkbox" value="'+subMenus[j]["Id"]+'" id="'+menus[i]["Id"]+'switch'+subMenus[j]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" switch="bool" checked/><label for="'+menus[i]["Id"]+'switch'+subMenus[j]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" id="switch'+subMenus[j]["Nombre"]+subMenus[j]["Id"]+'" data-on-label="Yes" data-off-label="No"></label></div></div></li>');
									h = menusRol.length;
								}else{
									if(h== menusRol.length-1){
										$("#"+ides).append('<li class="list-group-item"><div class="row align-items-start"><div class="col">'+subMenus[j]["Nombre"]+'</div><div class="col"><input type="checkbox" value="'+subMenus[j]["Id"]+'" id="'+menus[i]["Id"]+'switch'+subMenus[j]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" switch="bool"/><label for="'+menus[i]["Id"]+'switch'+subMenus[j]["Nombre"]+'switch'+menusRol[h]["IdReferencia"]+'" id="switch'+subMenus[j]["Nombre"]+subMenus[j]["Id"]+'" data-on-label="Yes" data-off-label="No"></label></div></div></li>');
									}
								}
							}
						}
					}
				}
				
				$('input[type=checkbox]').on('change', function() {
					var campoS = "";
					var valMenuP = $(this).prop("id");
					var splitMenuP = valMenuP.split("switch");
					var actionM = ""
					var valMenuS = $(this).val();
					if ($(this).is(':checked') ) {
						actionM = "Seleccionado";
						var datos = [idGes,valMenuS,actionM];
      					Datos("createSubMenu",datos,1);
					} else {
						actionM = "Deseleccionado";
						var datos = [splitMenuP[2],"",actionM];
      					Datos("createSubMenu",datos,1);
					}
				});
			}
	    });

		function Datos(metodo, parametros, combo) {
		//Ajax normal
		//cargar los selectores
		var metod = metodo;
		var params = parametros;
		var comb = combo;
		var menuruta = $("#menustext").val();

		//Envio de informacion al controller
		if(comb > 0){
		$.ajax({
			url: "index.php",
			type: 'post',
			data: {
			"c" : "rols",
			"m": metod,
			"p" : params
			},
			success: function(res) { 
			
			}
		});
		//Cargar combo
		}else if(comb != null || comb != ""){
		$.ajax({
			url: "index.php",
			type: 'post',
			data: {
			"c" : "menu",
			"m": metod,
			"p" : params
			},
			success: function(res) { 
			$("#"+comb+"").html(res);
			}
		});
		return;
		}   
	}
	});
</script>

<style type="text/css">

	/**
	 * The switch widget!
	 * Usage:
	 *   <input type="checkbox" switch[="type"] id="toggleSwitch" />
	 *   <label for="toggleSwitch" data-on-label="On" data-off-label="Off"></label>
	 */
	input[switch] {
	  display: none;
	}
	input[switch] + label {
	  font-size: 1em;
	  line-height: 1;
	  width: 4rem;
	  height: 2rem;
	  background-color: #ddd;
	  background-image: none;
	  border-radius: 2rem;
	  padding: 0.1666666667rem;
	  cursor: pointer;
	  display: inline-block;
	  text-align: center;
	  position: relative;
	  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2) inset;
	  font-family: inherit;
	  transition: all 0.1s ease-in-out;
	}
	input[switch] + label:before {
	  /* Label */
	  text-transform: uppercase;
	  color: #b7b7b7;
	  content: attr(data-off-label);
	  display: block;
	  font-family: inherit;
	  font-family: FontAwesome, inherit;
	  font-weight: 500;
	  font-size: 0.7rem;
	  line-height: 1.74rem;
	  position: absolute;
	  right: 0.2166666667rem;
	  margin: 0.2166666667rem;
	  top: 0;
	  text-align: center;
	  min-width: 1.6666666667rem;
	  overflow: hidden;
	  transition: all 0.1s ease-in-out;
	}
	input[switch] + label:after {
	  /* Slider */
	  content: "";
	  position: absolute;
	  left: 0.1666666667rem;
	  background-color: #f7f7f7;
	  box-shadow: none;
	  border-radius: 2rem;
	  height: 1.6666666667rem;
	  width: 1.6666666667rem;
	  transition: all 0.1s ease-in-out;
	}
	input[switch]:checked + label {
	  background-color: lightblue;
	  background-image: linear-gradient(rgba(255, 255, 255, 0.15), rgba(0, 0, 0, 0.2));
	  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.3) inset;
	}
	input[switch]:checked + label:before {
	  color: #fff;
	  content: attr(data-on-label);
	  right: auto;
	  left: 0.2166666667rem;
	}
	input[switch]:checked + label:after {
	  left: 2.1666666667rem;
	  background-color: #f7f7f7;
	  box-shadow: 1px 1px 10px 0 rgba(0, 0, 0, 0.3);
	}

	input[switch=bool] + label {
	  background-color: #ee6562;
	}
	input[switch=bool] + label:before {
	  color: #fff !important;
	}
	input[switch=bool]:checked + label {
	  background-color: #BCE954;
	}
	input[switch=bool]:checked + label:before {
	  color: #fff !important;
	}

	input[switch=default]:checked + label {
	  background-color: #a2a2a2;
	}
	input[switch=default]:checked + label:before {
	  color: #fff !important;
	}

	input[switch=success]:checked + label {
	  background-color: #BCE954;
	}
	input[switch=success]:checked + label:before {
	  color: #fff !important;
	}

	input[switch=warning]:checked + label {
	  background-color: gold;
	}
	input[switch=warning]:checked + label:before {
	  color: #fff !important;
	}
</style>