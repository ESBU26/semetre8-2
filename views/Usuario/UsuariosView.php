

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle">Crear Usuario</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="text" name="UsuariosDocumentos" class="inputform" id="UsuariosDocumentos" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Documento</label>
        </div>
        <div class="grupo">
          <input type="hidden" name="UsuariosId" class="inputform" id="UsuariosId" required autocomplete="off">
          <input type="text" name="UsuariosNombre" class="inputform" id="UsuariosNombre" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre del usuario</label>
        </div>

        <div class="grupo">
          <input type="text" name="UsuariosApellido" class="inputform" id="UsuariosApellido" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Apellido</label>
        </div>
        <div class="grupo">
          <input type="text" name="UsuariosCorreo" class="inputform" id="UsuariosCorreo" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Correo</label>
        </div>
        <div class="grupo">
          <input type="text" name="UsuariosTelefono" class="inputform" id="UsuariosTelefono" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Telefono</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="RolesCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Rol</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="TipoVinculacionCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Tipo de vinculación</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="EstadoCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Estado</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn closes btn-danger" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-primary">Guardar</button>
        <button type="button" id="GuardarEdit" style="display: none" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

  <!--Tablas-->
  <div class="card shadow mb-4 mt-4">
    <div class="card-header">
      <h6 class="m-0 font-weight-bold text-primary">Usuarios del sistema</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="TablaMenu" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Documento</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellido</th>
              <th scope="col">Correo</th>
              <th scope="col">Telefono</th>
              <th scope="col">Rol</th>
              <th scope="col">Vinculación</th>
              <th scope="col">Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="body_Menu">
                 
          </tbody>
        </table>
      </div>
    </div>
  </div>


<script type="text/javascript">

  //Cargar datos al iniciar la pagina
  $(document).ready(function () {

    var url2 = "index.php";

    //Ajax con datatable
        $('#TablaMenu').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "usuario",
              "m" : "listar",
              "p" : 0
            },
          },
          "columns":[
            {"data":"UsuariosId"},
            {"data":"UsuariosDocumentos"},
            {"data":"UsuariosNombre"},
            {"data":"UsuariosApellido"},
            {"data":"UsuariosCorreo"},
            {"data":"UsuariosTelefono"},
            {"data":"RolNombre"},
            {"data":"Nombre"},
            {"data":"EstadoNombre"},
            {"data":"acciones"}
          ],
          
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Agregar',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          PopNew();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ] 
        });
        CargarCombo();
  });


    //Abrir modal
    function PopNew() {
      $("#UsuariosDocumentos").val("");
      $("#UsuariosNombre").val("");
      $("#UsuariosApellido").val("");
      $("#UsuariosCorreo").val("");
      $("#UsuariosTelefono").val("");
      $("#RolesCombo").val("");
      $("#TipoVinculacionCombo").val("");
      $("#EstadoCombo").val("");
      $('#exampleModalCenter').modal('show');
      document.getElementById("GuardarNew").style.display = "block";
      document.getElementById("GuardarEdit").style.display = "none";
    };

    //Crear usuario
    $("#GuardarNew").click(function () {
      var UsuariosDocumentos = $("#UsuariosDocumentos").val();
      var UsuariosNombre = $("#UsuariosNombre").val();
      var UsuariosApellido = $("#UsuariosApellido").val();
      var UsuariosCorreo = $("#UsuariosCorreo").val();
      var UsuariosContrasena = $("#UsuariosDocumentos").val();
      var UsuariosTelefono = $("#UsuariosTelefono").val();
      var Rol = $("#RolesCombo").val();
      var TipoVinculacion = $("#TipoVinculacionCombo").val();
      var Estado = $("#EstadoCombo").val();
      var datos = [UsuariosNombre,UsuariosApellido,UsuariosDocumentos,UsuariosCorreo,UsuariosContrasena,UsuariosTelefono,Rol,TipoVinculacion,Estado];
      Datos("createUsuario",datos,1);
    });

    //Editar usuario
    $("#GuardarEdit").click(function () {

      var id = $("#UsuariosId").val();
      var UsuariosDocumentos = $("#UsuariosDocumentos").val();
      var UsuariosNombre = $("#UsuariosNombre").val();
      var UsuariosApellido = $("#UsuariosApellido").val();
      var UsuariosCorreo = $("#UsuariosCorreo").val();
      var UsuariosTelefono = $("#UsuariosTelefono").val();
      var Rol = $("#RolesCombo").val();
      var TipoVinculacion = $("#TipoVinculacionCombo").val();
      var Estado = $("#EstadoCombo").val();
      var datos = [id,UsuariosNombre,UsuariosApellido,UsuariosDocumentos,UsuariosCorreo,UsuariosTelefono,Rol,TipoVinculacion,Estado];
      Datos("editUsuario",datos,1);
    });


    //Se captura el ID y se llama la funcion Datos
    //Se envia el metodo del controller y se envia el id
    function Eliminar(id) {
      Datos("deleteUsuario", id,1);
    }

    function Modificar(id) {
      $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "usuario",
            "m": "ConsultaUsuario",
            "p" : id
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            $("#UsuariosId").val(prueba["UsuariosId"]);
            $("#UsuariosDocumentos").val(prueba["UsuariosDocumentos"]);
            $("#UsuariosNombre").val(prueba["UsuariosNombre"]);
            $("#UsuariosApellido").val(prueba["UsuariosApellido"]);
            $("#UsuariosCorreo").val(prueba["UsuariosCorreo"]);
            $("#UsuariosTelefono").val(prueba["UsuariosTelefono"]);
            $("#RolesCombo").val(prueba["roles_RolId"]);
            $("#TipoVinculacionCombo").val(prueba["tiposvinculacion_Id"]);
            $("#EstadoCombo").val(prueba["estado_EstadoId"]);
            
            document.getElementById("GuardarNew").style.display = "none";
            document.getElementById("GuardarEdit").style.display = "block";

            $('#exampleModalCenter').modal('show');
         }
      });
    }

    function Datos(metodo, parametros, combo) {
      //Ajax normal
      //cargar los selectores
      var metod = metodo;
      var params = parametros;
      var comb = combo;
      var menuruta = $("#menustext").val();

      //Envio de informacion al controller
      if(comb > 0){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "usuario",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           if(res != 0){
              $('#exampleModalCenter').modal('hide');
              alertify.success('Datos guardados con exito');
              setTimeout(function(){redirect(menuruta); }, 1000);
            }else{
              alertify.error("No se pudo guardar los datos!!");
            }
            
          }
        });
        //Cargar combo
      }else if(comb != null || comb != ""){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           $("#"+comb+"").html(res);
          }
        });
        return;
      }   
    }

    function CargarCombo() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "usuario",
          "m": "listarCombos",
          "p" : 0
        },
        success: function(res) { 
        var respuesta = JSON.parse(res);
        $("#EstadoCombo").empty();
        $("#RolesCombo").empty();
        $("#TipoVinculacionCombo").empty();

        $("#EstadoCombo").append("<option selected>Seleccionar...</option>");
        $("#TipoVinculacionCombo").append("<option selected>Seleccionar...</option>");
        $("#RolesCombo").append("<option selected>Seleccionar...</option>");
       

        for (var i = 0; i < respuesta["Estados"].length; i++) {
          $Estados = respuesta["Estados"][i];
          $("#EstadoCombo").append("<option value='"+$Estados["EstadoId"]+"'>"+$Estados["EstadoNombre"]+"</option>");
        }

        for (var i = 0; i < respuesta["TiposVinculacion"].length; i++) {
          $TiposVinculacion = respuesta["TiposVinculacion"][i];
          $("#TipoVinculacionCombo").append("<option value='"+$TiposVinculacion["Id"]+"'>"+$TiposVinculacion["Nombre"]+"</option>");
        }

        for (var i = 0; i < respuesta["Roles"].length; i++) {
          $Roles = respuesta["Roles"][i];
          $("#RolesCombo").append("<option value='"+$Roles["RolId"]+"'>"+$Roles["RolNombre"]+"</option>");
        }
      }
      });
  }

  $(".closes").click(function () {
    $('#exampleModalCenter').modal('hide');
  });
</script>
