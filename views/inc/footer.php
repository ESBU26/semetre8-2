
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript-->

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/js/jquery.min.js"></script>
    <script src="<?php echo RUTA_URL;?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo RUTA_URL;?>assets/js/js/sb-admin-2.min.js"></script>

    <script src="<?php echo RUTA_URL;?>assets/Datatables/datatables/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        var menu= '';
        var urr = '';
        function redirect(menu) {
            var espera =4000;
            urr = "<?php echo RUTA_URL;?>";
            url2 = urr+"views/"+menu;
         
            $.ajax({
                url: url2,
                timeout: 30000,
                beforeSend : function () {
                    $("#principal").html('<div class="cssload-loader"><div class="cssload-dot"></div><div class="cssload-dot"></div><div class="cssload-dot"></div><div class="cssload-dot"></div><div class="cssload-dot"></div></div>');
                },
                success: function (data) {
                    setTimeout(function () {
                        $("#principal").html(data);
                        $("#menusDB").html("<input type='hidden' name='menus' id='menustext' value='"+menu+"'/>");
                    });
                }
            });

        }
    </script>


    <style type="text/css">
        .cssload-loader * {
        box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .cssload-loader {
            position: absolute;
            left: 50%;
            
            width: 425px;
            height: 425px;
            
            margin-top: auto;
            margin-left: -213px;
            
            perspective: 213px;
                -o-perspective: 213px;
                -ms-perspective: 213px;
                -webkit-perspective: 213px;
                -moz-perspective: 213px;
            transform-type: preserve-3d;
                -o-transform-type: preserve-3d;
                -ms-transform-type: preserve-3d;
                -webkit-transform-type: preserve-3d;
                -moz-transform-type: preserve-3d;
            
            animation: cssload-loader 6.9s cubic-bezierrgb(0,0,0) infinite;
                -o-animation: cssload-loader 6.9s cubic-bezierrgb(0,0,0) infinite;
                -ms-animation: cssload-loader 6.9s cubic-bezierrgb(0,0,0) infinite;
                -webkit-animation: cssload-loader 6.9s cubic-bezierrgb(0,0,0) infinite;
                -moz-animation: cssload-loader 6.9s cubic-bezierrgb(0,0,0) infinite;
        }




        .cssload-dot {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 10;

            width: 43px;
            height: 43px;
            
            margin-top: -191px;
            margin-left: -21px;

            border-radius: 21px;

            background-color: rgb(30,63,87);

            transform-type: preserve-3d;
                -o-transform-type: preserve-3d;
                -ms-transform-type: preserve-3d;
                -webkit-transform-type: preserve-3d;
                -moz-transform-type: preserve-3d;
            transform-origin: 50% 191px;
                -o-transform-origin: 50% 191px;
                -ms-transform-origin: 50% 191px;
                -webkit-transform-origin: 50% 191px;
                -moz-transform-origin: 50% 191px;
            transform: rotateZ(30deg);
                -o-transform: rotateZ(30deg);
                -ms-transform: rotateZ(30deg);
                -webkit-transform: rotateZ(30deg);
                -moz-transform: rotateZ(30deg);

            animation: cssload-dot1 1.73s cubic-bezier(.6,0,.4,1) infinite;
                -o-animation: cssload-dot1 1.73s cubic-bezier(.6,0,.4,1) infinite;
                -ms-animation: cssload-dot1 1.73s cubic-bezier(.6,0,.4,1) infinite;
                -webkit-animation: cssload-dot1 1.73s cubic-bezier(.6,0,.4,1) infinite;
                -moz-animation: cssload-dot1 1.73s cubic-bezier(.6,0,.4,1) infinite;
        }

        .cssload-dot:nth-child(2) {
            transform: rotateZ(15deg);
                -o-transform: rotateZ(15deg);
                -ms-transform: rotateZ(15deg);
                -webkit-transform: rotateZ(15deg);
                -moz-transform: rotateZ(15deg);
            animation-name: cssload-dot2;
                -o-animation-name: cssload-dot2;
                -ms-animation-name: cssload-dot2;
                -webkit-animation-name: cssload-dot2;
                -moz-animation-name: cssload-dot2;
            animation-delay: 172.5ms;
                -o-animation-delay: 172.5ms;
                -ms-animation-delay: 172.5ms;
                -webkit-animation-delay: 172.5ms;
                -moz-animation-delay: 172.5ms;
            background-color: rgb(45,85,109);
        }

        .cssload-dot:nth-child(3) {
            transform: rotateZ(0deg);
                -o-transform: rotateZ(0deg);
                -ms-transform: rotateZ(0deg);
                -webkit-transform: rotateZ(0deg);
                -moz-transform: rotateZ(0deg);
            animation-name: cssload-dot3;
                -o-animation-name: cssload-dot3;
                -ms-animation-name: cssload-dot3;
                -webkit-animation-name: cssload-dot3;
                -moz-animation-name: cssload-dot3;
            animation-delay: 345ms;
                -o-animation-delay: 345ms;
                -ms-animation-delay: 345ms;
                -webkit-animation-delay: 345ms;
                -moz-animation-delay: 345ms;
            background-color: rgb(68,120,145);
        }

        .cssload-dot:nth-child(4) {
            transform: rotateZ(-15deg);
                -o-transform: rotateZ(-15deg);
                -ms-transform: rotateZ(-15deg);
                -webkit-transform: rotateZ(-15deg);
                -moz-transform: rotateZ(-15deg);
            animation-name: cssload-dot4;
                -o-animation-name: cssload-dot4;
                -ms-animation-name: cssload-dot4;
                -webkit-animation-name: cssload-dot4;
                -moz-animation-name: cssload-dot4;
            animation-delay: 517.5ms;
                -o-animation-delay: 517.5ms;
                -ms-animation-delay: 517.5ms;
                -webkit-animation-delay: 517.5ms;
                -moz-animation-delay: 517.5ms;
            background-color: rgb(89,152,178);
        }

        .cssload-dot:nth-child(5) {
            transform: rotateZ(-30deg);
                -o-transform: rotateZ(-30deg);
                -ms-transform: rotateZ(-30deg);
                -webkit-transform: rotateZ(-30deg);
                -moz-transform: rotateZ(-30deg);
            animation-name: cssload-dot5;
                -o-animation-name: cssload-dot5;
                -ms-animation-name: cssload-dot5;
                -webkit-animation-name: cssload-dot5;
                -moz-animation-name: cssload-dot5;
            animation-delay: 690ms;
                -o-animation-delay: 690ms;
                -ms-animation-delay: 690ms;
                -webkit-animation-delay: 690ms;
                -moz-animation-delay: 690ms;
            background-color: rgb(107,178,205);
        }











        @keyframes cssload-loader {
            0% {
                transform: rotateX(30deg) rotateZ(0deg);
            }
            100% {
                transform: rotateX(30deg) rotateZ(-360deg);
            }
        }

        @-o-keyframes cssload-loader {
            0% {
                -o-transform: rotateX(30deg) rotateZ(0deg);
            }
            100% {
                -o-transform: rotateX(30deg) rotateZ(-360deg);
            }
        }

        @-ms-keyframes cssload-loader {
            0% {
                -ms-transform: rotateX(30deg) rotateZ(0deg);
            }
            100% {
                -ms-transform: rotateX(30deg) rotateZ(-360deg);
            }
        }

        @-webkit-keyframes cssload-loader {
            0% {
                -webkit-transform: rotateX(30deg) rotateZ(0deg);
            }
            100% {
                -webkit-transform: rotateX(30deg) rotateZ(-360deg);
            }
        }

        @-moz-keyframes cssload-loader {
            0% {
                -moz-transform: rotateX(30deg) rotateZ(0deg);
            }
            100% {
                -moz-transform: rotateX(30deg) rotateZ(-360deg);
            }
        }

        @keyframes cssload-dot1 {
            0% {
                transform: rotateZ(30deg) rotateX(10deg);
            }
            95%, 100% {
                transform: rotateZ(390deg) rotateX(10deg);
            }
        }

        @-o-keyframes cssload-dot1 {
            0% {
                -o-transform: rotateZ(30deg) rotateX(10deg);
            }
            95%, 100% {
                -o-transform: rotateZ(390deg) rotateX(10deg);
            }
        }

        @-ms-keyframes cssload-dot1 {
            0% {
                -ms-transform: rotateZ(30deg) rotateX(10deg);
            }
            95%, 100% {
                -ms-transform: rotateZ(390deg) rotateX(10deg);
            }
        }

        @-webkit-keyframes cssload-dot1 {
            0% {
                -webkit-transform: rotateZ(30deg) rotateX(10deg);
            }
            95%, 100% {
                -webkit-transform: rotateZ(390deg) rotateX(10deg);
            }
        }

        @-moz-keyframes cssload-dot1 {
            0% {
                -moz-transform: rotateZ(30deg) rotateX(10deg);
            }
            95%, 100% {
                -moz-transform: rotateZ(390deg) rotateX(10deg);
            }
        }

        @keyframes cssload-dot2 {
            0% {
                transform: rotateZ(15deg) rotateX(10deg);
            }
            95%, 100% {
                transform: rotateZ(375deg) rotateX(10deg);
            }
        }

        @-o-keyframes cssload-dot2 {
            0% {
                -o-transform: rotateZ(15deg) rotateX(10deg);
            }
            95%, 100% {
                -o-transform: rotateZ(375deg) rotateX(10deg);
            }
        }

        @-ms-keyframes cssload-dot2 {
            0% {
                -ms-transform: rotateZ(15deg) rotateX(10deg);
            }
            95%, 100% {
                -ms-transform: rotateZ(375deg) rotateX(10deg);
            }
        }

        @-webkit-keyframes cssload-dot2 {
            0% {
                -webkit-transform: rotateZ(15deg) rotateX(10deg);
            }
            95%, 100% {
                -webkit-transform: rotateZ(375deg) rotateX(10deg);
            }
        }

        @-moz-keyframes cssload-dot2 {
            0% {
                -moz-transform: rotateZ(15deg) rotateX(10deg);
            }
            95%, 100% {
                -moz-transform: rotateZ(375deg) rotateX(10deg);
            }
        }

        @keyframes cssload-dot3 {
            0% {
                transform: rotateZ(0deg) rotateX(10deg);
            }
            95%, 100% {
                transform: rotateZ(360deg) rotateX(10deg);
            }
        }

        @-o-keyframes cssload-dot3 {
            0% {
                -o-transform: rotateZ(0deg) rotateX(10deg);
            }
            95%, 100% {
                -o-transform: rotateZ(360deg) rotateX(10deg);
            }
        }

        @-ms-keyframes cssload-dot3 {
            0% {
                -ms-transform: rotateZ(0deg) rotateX(10deg);
            }
            95%, 100% {
                -ms-transform: rotateZ(360deg) rotateX(10deg);
            }
        }

        @-webkit-keyframes cssload-dot3 {
            0% {
                -webkit-transform: rotateZ(0deg) rotateX(10deg);
            }
            95%, 100% {
                -webkit-transform: rotateZ(360deg) rotateX(10deg);
            }
        }

        @-moz-keyframes cssload-dot3 {
            0% {
                -moz-transform: rotateZ(0deg) rotateX(10deg);
            }
            95%, 100% {
                -moz-transform: rotateZ(360deg) rotateX(10deg);
            }
        }

        @keyframes cssload-dot4 {
            0% {
                transform: rotateZ(-15deg) rotateX(10deg);
            }
            95%, 100% {
                transform: rotateZ(345deg) rotateX(10deg);
            }
        }

        @-o-keyframes cssload-dot4 {
            0% {
                -o-transform: rotateZ(-15deg) rotateX(10deg);
            }
            95%, 100% {
                -o-transform: rotateZ(345deg) rotateX(10deg);
            }
        }

        @-ms-keyframes cssload-dot4 {
            0% {
                -ms-transform: rotateZ(-15deg) rotateX(10deg);
            }
            95%, 100% {
                -ms-transform: rotateZ(345deg) rotateX(10deg);
            }
        }

        @-webkit-keyframes cssload-dot4 {
            0% {
                -webkit-transform: rotateZ(-15deg) rotateX(10deg);
            }
            95%, 100% {
                -webkit-transform: rotateZ(345deg) rotateX(10deg);
            }
        }

        @-moz-keyframes cssload-dot4 {
            0% {
                -moz-transform: rotateZ(-15deg) rotateX(10deg);
            }
            95%, 100% {
                -moz-transform: rotateZ(345deg) rotateX(10deg);
            }
        }

        @keyframes cssload-dot5 {
            0% {
                transform: rotateZ(-30deg) rotateX(10deg);
            }
            95%, 100% {
                transform: rotateZ(330deg) rotateX(10deg);
            }
        }

        @-o-keyframes cssload-dot5 {
            0% {
                -o-transform: rotateZ(-30deg) rotateX(10deg);
            }
            95%, 100% {
                -o-transform: rotateZ(330deg) rotateX(10deg);
            }
        }

        @-ms-keyframes cssload-dot5 {
            0% {
                -ms-transform: rotateZ(-30deg) rotateX(10deg);
            }
            95%, 100% {
                -ms-transform: rotateZ(330deg) rotateX(10deg);
            }
        }

        @-webkit-keyframes cssload-dot5 {
            0% {
                -webkit-transform: rotateZ(-30deg) rotateX(10deg);
            }
            95%, 100% {
                -webkit-transform: rotateZ(330deg) rotateX(10deg);
            }
        }

        @-moz-keyframes cssload-dot5 {
            0% {
                -moz-transform: rotateZ(-30deg) rotateX(10deg);
            }
            95%, 100% {
                -moz-transform: rotateZ(330deg) rotateX(10deg);
            }
        }
    </style>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/js/js/alertify.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/Buttons-1.7.0/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/Buttons-1.7.0/js/buttons.colVis.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/JSZip-2.5.0/jszip.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/pdfmake-0.1.36/pdfmake.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/pdfmake-0.1.36/vfs_fonts.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/Buttons-1.7.0/js/buttons.html5.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo RUTA_URL;?>assets/Datatables/datatables/dataTables.bootstrap4.min.js"></script>
    


    

</body>

</html>