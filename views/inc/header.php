<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Proyecto</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo RUTA_URL;?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <!-- Custom styles for this template-->
    <link href="<?php echo RUTA_URL;?>assets/css/sb-admin-2.min.css" rel="stylesheet">
  
    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Css/css/alertify/alertify.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Css/css/alertify/alertify.css">

    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Css/css/estilospropios.css">

    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Datatables/datatables/dataTables.bootstrap4.min.css"> 

    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Datatables/Buttons-1.7.0/css/buttons.bootstrap4.min.css"> 


    <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>assets/Css/css/bootstrap.css"> 
    
</head>
