 


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle">Crear Grupo</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="text" name="GrupoCodigo" class="inputform" id="GrupoCodigo" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Codigo del grupo</label>
        </div>
        <div class="grupo">
          <input type="hidden" name="GrupoId" class="inputform" id="GrupoId" required autocomplete="off">
          <input type="text" name="GrupoNombre" class="inputform" id="GrupoNombre" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre del grupo</label>
        </div>

        <div class="grupo">
          <input type="text" name="GrupoCapacidad" class="inputform" id="GrupoCapacidad" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Capacidad del grupo</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="UsuariosCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Docentes</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn closes btn-danger" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-primary">Guardar</button>
        <button type="button" id="GuardarEdit" style="display: none" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

  <!--Tablas-->
  <div class="card shadow mb-4 mt-4">
    <div class="card-header">
      <h6 class="m-0 font-weight-bold text-primary">Grupos</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="TablaMenu" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Codigo</th>
              <th scope="col">Nombre</th>
              <th scope="col">Capacidad</th>
              <th scope="col">Documento</th>
              <th scope="col">Nombes</th>
              <th scope="col">Apellidos</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="body_Menu">
                 
          </tbody>
        </table>
      </div>
    </div>
  </div>


<script type="text/javascript">

  //Cargar datos al iniciar la pagina
  $(document).ready(function () {

    var url2 = "index.php";

    //Ajax con datatable
        $('#TablaMenu').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "grupos",
              "m" : "listar",
              "p" : 0
            },
          },
          "columns":[
            {"data":"GrupoId"},
            {"data":"GrupoCodigo"},
            {"data":"GrupoNombre"},
            {"data":"GrupoCapacidad"},
            {"data":"UsuariosDocumentos"},
            {"data":"UsuariosNombre"},
            {"data":"UsuariosApellido"},
            {"data":"acciones"}
          ],
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Agregar',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          PopNew();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ] 
        });
        CargarCombo();
  });


    //Abrir modal
    function PopNew() {
      $("#GrupoCodigo").val("");
      $("#GrupoNombre").val("");
      $("#GrupoCapacidad").val("");
      $("#UsuariosCombo").val("");
      $('#exampleModalCenter').modal('show');
      document.getElementById("GuardarNew").style.display = "block";
      document.getElementById("GuardarEdit").style.display = "none";
    };

    //Crear Grupo
    $("#GuardarNew").click(function () {
      var GrupoCodigo = $("#GrupoCodigo").val();
      var GrupoNombre = $("#GrupoNombre").val();
      var GrupoCapacidad = $("#GrupoCapacidad").val();
      var Usuario = $("#UsuariosCombo").val();
      var datos = [GrupoCodigo,GrupoNombre,GrupoCapacidad,Usuario];
      Datos("createGrupo",datos,1);
    });

    //Editar Grupo
    $("#GuardarEdit").click(function () {

      var id = $("#GrupoId").val();
      var GrupoCodigo = $("#GrupoCodigo").val();
      var GrupoNombre = $("#GrupoNombre").val();
      var GrupoCapacidad = $("#GrupoCapacidad").val();
      var Usuario = $("#UsuariosCombo").val();
      var datos = [id,GrupoCodigo,GrupoNombre,GrupoCapacidad,Usuario];
      Datos("editGrupo",datos,1);
    });


    //Se captura el ID y se llama la funcion Datos
    //Se envia el metodo del controller y se envia el id
    function Eliminar(id) {
      Datos("deleteUsuario", id,1);
    }

    function Modificar(id) {
      $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "grupos",
            "m": "ConsultaGrupo",
            "p" : id
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            $("#GrupoId").val(prueba["GrupoId"]);
            $("#GrupoCodigo").val(prueba["GrupoCodigo"]);
            $("#GrupoNombre").val(prueba["GrupoNombre"]);
            $("#GrupoCapacidad").val(prueba["GrupoCapacidad"]);
            $("#UsuariosCombo").val(prueba["UsuariosId"]);
            
            document.getElementById("GuardarNew").style.display = "none";
            document.getElementById("GuardarEdit").style.display = "block";

            $('#exampleModalCenter').modal('show');
         }
      });
    }

    function Datos(metodo, parametros, combo) {
      //Ajax normal
      //cargar los selectores
      var metod = metodo;
      var params = parametros;
      var comb = combo;
      var menuruta = $("#menustext").val();

      //Envio de informacion al controller
      if(comb > 0){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "grupos",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           if(res != 0){
              $('#exampleModalCenter').modal('hide');
              alertify.success('Datos guardados con exito');
              setTimeout(function(){redirect(menuruta); }, 1000);
            }else{
              alertify.error("No se pudo guardar los datos!!");
            }
            
          }
        });
        //Cargar combo
      }else if(comb != null || comb != ""){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "menu",
            "m": metod,
            "p" : params
          },
          success: function(res) { 
           $("#"+comb+"").html(res);
          }
        });
        return;
      }   
    }

    function CargarCombo() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "grupos",
          "m": "listarCombos",
          "p" : 0
        },
        success: function(res) { 
            var respuesta = JSON.parse(res);
            $("#UsuariosCombo").empty();

            $("#UsuariosCombo").append("<option selected>Seleccionar...</option>");

            for (var i = 0; i < respuesta["Usuarios"].length; i++) {
            $Usuarios = respuesta["Usuarios"][i];
            $("#UsuariosCombo").append("<option value='"+$Usuarios["UsuariosId"]+"'>"+$Usuarios["UsuariosNombre"]+"</option>");
            }
        }
        });
    }
    
    $(".closes").click(function () {
        $('#exampleModalCenter').modal('hide');
    });
</script>
