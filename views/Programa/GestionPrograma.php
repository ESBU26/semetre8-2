

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle1">Crear Programas</h5>
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle2" style="display: none">Modificar Programas</h5>
        <button type="button" class="close closes" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
        <div class="grupo">
          <input type="hidden" name="id" class="inputform" id="id" required autocomplete="off">
          <input type="text" name="nombre" class="inputform" id="NombrePrograma" required autocomplete="off"><span class="barra"></span>
          <label class="labelsform">Nombre del programas</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="FacultadCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Facultad</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="PeriodoCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Periodo</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="TipoProgramaCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Tipo de programa</label>
        </div>
        <div class="grupo">
          <select class="inputform" name="padre" id="EstadoCombo" required>
            
          </select><span class="barra"></span>
          <label class="labelsform">Estado</label>
        </div>

        <div class="grupo">
          <textarea class="inputform" id="Observaciones"></textarea>
          <span class="barra"></span>
          <label for="" class="labelsform">Comments</label>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn closes btn-danger" data-dismiss="modal" id="cancelarModal">Cancelar</button>
        <button type="button" id="GuardarNew" class="btn btn-primary">Guardar</button>
        <button type="button" id="GuardarEdit" onclick="GuardarCambios()" style="display: none" class="btn btn-success">Modificar</button>
      </div>
    </div>
  </div>
</div>

  <!--Opciones a seleccionar-->
  <div class="col-xl-6 mb-4">
    <div class="row">
        <label for="periodo">Periodo</label>
        <select name="periodo" id="selectPeriodo" class="form-select col-md-10">
             <option selected>Select</option>
        </select>
        <button id="buscar" class="btn btn-primary col-md-1 ml-1"><span class="fa fa-search"></span></button>
    </div>
  </div>




  <!--Tablas-->
  <div class="card shadow mb-4 mt-4">
    <div class="card-header">
      <h6 class="m-0 font-weight-bold text-primary">Menus del sistema</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="TablaMenu" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre del Programa</th>
              <th scope="col">Descripcción</th>
              <th scope="col">Facultad</th>
              <th scope="col">Tipo</th>
              <th scope="col">Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="body_Menu">
                 
          </tbody>
        </table>
      </div>
    </div>
  </div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenterUno" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary" id="exampleModalCenterTitle1">Gestión Materias</h5>
        <button type="button" class="close closesMa" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form">
          <div class="row mb-2">
            <select id="select_mate" class="form-select ml-3 col-md-10">
              <option>Seleccionar...</option>
            </select>
            <button onclick="agregarMatePro()" class="btn btn-success ml-1 col-md-1">
              <span>
                <i class="fas fa-plus"></i>
              </span>
            </button>
          </div>
          <input type="hidden" name="pro" id="programaId">
          <div>

            <table class="table table-bordered" id="Materias" width="100%">
              <thead>
                <tr>
                  <th scope="col">Codigo</th>
                  <th scope="col">Materia</th>
                  <th scope="col">Semestre</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody id="BodyMatePo">
                     
              </tbody>
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="closesMa btn btn-danger">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function () {
        var url2 = "index.php";
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "programas",
            "m": "CargarPeriodo",
            "p" : 0
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            for(var i =0; i< prueba.length;i++){
               $("#selectPeriodo").append("<option value='"+prueba[i]['PeriodoId']+"'>"+prueba[i]["NombrePeriodo"]+"</option>"); 
               
            }            
          }
        });
        CargarCombo();
        CargarTabla();
    });

  var tabla;

  $("#buscar").click(function () {
    tabla.destroy();
    CargarTabla();
  });


//Cargar Modal para agregarle materias al programa
  function AgregarMate(id) {
    $("#programaId").val(id);
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "CargarMaterias",
        "p" : id
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if(respuesta["Materias"].length>0){
           $("#BodyMatePo").empty();
           $("#select_mate").empty();
          for(var i =0; i< respuesta["Materias"].length;i++){
            $("#select_mate").append("<option value='"+respuesta[["Materias"]][i]['MateriasId']+"'>Codigo:"+respuesta[["Materias"]][i]["MateriasCodigo"]+"  ----  Nombre:"+respuesta[["Materias"]][i]["MateriasNombre"]+" --- Semestre:"+respuesta[["Materias"]][i]["MateriasSemestre"]+"</option>"); 
          }

          for (var j =0; j< respuesta["MatePro"].length;j++) {
            $("#BodyMatePo").append("<tr><td>"+respuesta["MatePro"][j]["MateriasCodigo"]+"</td><td>"+respuesta["MatePro"][j]["MateriasNombre"]+"</td><td>"+respuesta["MatePro"][j]["MateriasSemestre"]+"</td><td><button onclick='elimiMatePro("+respuesta["MatePro"][j]["ProgramasMateriasId"]+")' class='btn btn-danger'><span><i class='fas fa-trash'></i></span></button></td></tr>")
          }
          $('#exampleModalCenterUno').modal('show');
        }else{
          alertify.error("¡No existen materias registradas!");
        }
      }
    });
  }

  $(".closesMa").click(function () {
    $('#exampleModalCenterUno').modal('hide');
  });

//Elimina materias del programa
  function elimiMatePro(id) {
    var progra = $("#programaId").val();
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "EliminarMatePro",
        "p" : id
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if (respuesta) {
          AgregarMate(progra);
          alertify.success("Materia eliminada con exito!!");
        }else{
          alertify.error(respuesta);
        }
      }
    });
  }

//Agrega Materias al programa
  function agregarMatePro() {
    var progra = $("#programaId").val();
    var mate = $("#select_mate").val();
    var dat = [progra, mate];
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "AgregaarMatePro",
        "p" : dat
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if (respuesta) {
          AgregarMate(progra);
          alertify.success("Materia agregada con exito!!");
        }else{
          if (respuesta==false){
            alertify.error("La materia ya esta asignada a este programa!!");
          }else{
            alertify.error(respuesta);
          }
        }
      }
    });

  }

  function CargarTabla() {
    var peri = $("#selectPeriodo").val();

    if(peri>=0){

    }else{
      peri = 1;
    }

    tabla = $('#TablaMenu').DataTable({
          "bDeferRender":true,
          "sPaginationType" : "full_numbers",
          "ajax":{
            "url": "index.php",
            "type": 'post',
            "data": {
              "c" : "programas",
              "m": "listar",
              "p" : peri
            },
            /*"dataSrc": function (json) {
               
              //alertify.success('Success message');
              console.log(json);
            },*/
          },
          "columns":[
            {"data":"ProgramasId"},
            {"data":"ProgramasNombre"},
            {"data":"ProgramasDescripcion"},
            {"data":"FacultadNombre"},
            {"data":"TipoProgramaNombre"},
            {"data":"EstadoNombre"},
            {"data":"acciones"},
          ],
          dom: 'Blfrtip',
          buttons: [
              {
                text: '<i class="fas fa-plus"></i>',
                titleAttr: 'Agregar',
                className: 'btn btn-primary mb-2',
                action:function ( e, dt, node, config ) {
                          PopNew();
                      }
              },
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success mb-2'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger mb-2'
              }
          ] 
        });
  }

  function PopNew() {
    document.getElementById("GuardarNew").style.display = "block";
    document.getElementById("GuardarEdit").style.display = "none";

    document.getElementById("exampleModalCenterTitle1").style.display = "block";
    document.getElementById("exampleModalCenterTitle2").style.display = "none";
    AbrirModal();
  };

  $("#GuardarNew").click(function () {
    ObtenerInfo();
    CrearNew();
  });

  function AbrirModal() {
    $("#NombrePrograma").val("");
    $("#FacultadCombo").val("");
    $("#PeriodoCombo").val("");
    $("#TipoProgramaCombo").val("");
    $("#EstadoCombo").val("");
    $("#Observaciones").val("");
    $('#exampleModalCenter').modal('show');
  }

  var datos;

  function ObtenerInfo() {
    var nombre = $("#NombrePrograma").val();
    var facultad =$("#FacultadCombo").val();
    var periodo = $("#PeriodoCombo").val();
    var tipoP = $("#TipoProgramaCombo").val();
    var estado = $("#EstadoCombo").val();
    var observ = $("#Observaciones").val();
    datos = [nombre,observ,periodo,tipoP,facultad,estado];
  }

  $(".closes").click(function () {
    $('#exampleModalCenter').modal('hide');
  });

  function CrearNew() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "programas",
          "m": "CrearNew",
          "p" : datos
        },
        success: function(res) { 
          var respuesta = JSON.parse(res);
          if(respuesta){
            alertify.success('¡Datos Guardados con exito!');
            tabla.destroy();
            CargarTabla();
            $('#exampleModalCenter').modal('hide');
          }else{
            alertify.error('¡Error al guardar la información!');
          }

        }
    });
    datos = [];
  }


  function CargarCombo() {
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "programas",
          "m": "listarCombos",
          "p" : 0
        },
        success: function(res) { 
        var respuesta = JSON.parse(res);
        $("#EstadoCombo").empty();
        $("#FacultadCombo").empty();
        $("#PeriodoCombo").empty();
        $("#TipoProgramaCombo").empty();

        $("#EstadoCombo").append("<option selected>Seleccionar...</option>");
        $("#FacultadCombo").append("<option selected>Seleccionar...</option>");
        $("#PeriodoCombo").append("<option selected>Seleccionar...</option>");
        $("#TipoProgramaCombo").append("<option selected>Seleccionar...</option>");


        

        for (var i = 0; i < respuesta["Estados"].length; i++) {
          $Estados = respuesta["Estados"][i];
          $("#EstadoCombo").append("<option value='"+$Estados["EstadoId"]+"'>"+$Estados["EstadoNombre"]+"</option>");
        }

        for (var i = 0; i < respuesta["Facultad"].length; i++) {
          $Facultad = respuesta["Facultad"][i];
          $("#FacultadCombo").append("<option value='"+$Facultad["FacultadId"]+"'>"+$Facultad["FacultadNombre"]+"</option>");
        }

        for (var i = 0; i < respuesta["Periodo"].length; i++) {
          $Periodo = respuesta["Periodo"][i];

          $("#PeriodoCombo").append("<option value='"+$Periodo["PeriodoId"]+"'>"+$Periodo["NombrePeriodo"]+"</option>");
        }

        for (var i = 0; i < respuesta["Tipo"].length; i++) {
          $Tipo = respuesta["Tipo"][i];
          $("#TipoProgramaCombo").append("<option value='"+$Tipo["TipoProgramaId"]+"'>"+$Tipo["TipoProgramaNombre"]+"</option>");
        }
      }
      });
  }

  function Modificar(id) {
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "BuscarPro",
        "p" : id
      },
      success: function(res) { 
        var prueba = JSON.parse(res);


        $("#NombrePrograma").val(prueba[0]["ProgramasNombre"]);
        $("#FacultadCombo").val(prueba[0]["FacultadId"]);
        $("#PeriodoCombo").val(prueba[0]["Periodo"]);
        $("#TipoProgramaCombo").val(prueba[0]["TipoProgramaId"]);
        $("#EstadoCombo").val(prueba[0]["estado_EstadoId"]);
        $("#Observaciones").val(prueba[0]["ProgramasDescripcion"]);
        $("#id").val(prueba[0]["ProgramasId"]);

        document.getElementById("GuardarNew").style.display = "none";
        document.getElementById("GuardarEdit").style.display = "block";

        document.getElementById("exampleModalCenterTitle1").style.display = "none";
        document.getElementById("exampleModalCenterTitle2").style.display = "block";

        

        $('#exampleModalCenter').modal('show');
      }
    });
  }

  function Eliminar(id) {
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "Eliminar",
        "p" : id
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if(respuesta == true){
          alertify.success('¡Datos Eliminados con exito!');
          tabla.destroy();
          CargarTabla();
        }else{
          alertify.error(respuesta);
        }
      }
    });
  }

  function GuardarCambios() {
    ObtenerInfo();
    var idm = $("#id").val();
    modi = [datos,idm];
    $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "programas",
          "m": "GuardarCambios",
          "p" : modi
        },
        success: function(res) { 
          var respuesta = JSON.parse(res);
          if(respuesta){
            alertify.success('¡Datos Guardados con exito!');
            tabla.destroy();
            CargarTabla();
            $('#exampleModalCenter').modal('hide');
          }else{
            alertify.error('¡Error al guardar la información!');
          }

        }
    });
    datos = [];
    modi = [];  
  }

</script>