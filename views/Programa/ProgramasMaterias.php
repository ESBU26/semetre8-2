<div class='row'>
    <button class='btn col-md-1' onclick='volverProgramas()' id='atras'>
        <span><i class='fs-1 fas fa-chevron-circle-left'></i></span>
    </button>
    <h4 class='col-md-11 text-center text-primary'>
        <strong></strong>
    </h4>
</div>
<div class="row mb-2">
  <select id="select_grupo" class="form-select ml-3 col-md-10">
    <option>Seleccionar...</option>
  </select>
  <button onclick="agregarGrupoPro()" class="btn btn-success ml-1 col-md-1">
    <span>
      <i class="fas fa-plus"></i>
    </span>
  </button>
</div>
<input type="hidden" name="pro" id="programaId">
<div>

  <table class="table table-bordered" id="Materias" width="100%">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Materia</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody id="BodyMatePo">
            
    </tbody>
  </table>
</div>

<script type="text/javascript">
  $(document).ready(function () {
      var url2 = "index.php";
      var materiaId = $("#menustext").val();
      var splitMateriaId = materiaId.split("=");
      var idMa = $("#programaId").val(splitMateriaId[1]);
      cargarGrupos(splitMateriaId[1]);
  });

  function obtenerValorParametro(sParametroNombre) {
  var sPaginaURL = window.location.search.substring(1);
  var sURLVariables = sPaginaURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var sParametro = sURLVariables[i].split('=');
      if (sParametro[0] == sParametroNombre) {
        return sParametro[1];
      }
    }
    //return null;
  }
  //Funcion para cargar el combo de periodo
  function cargarGrupos(id){
      $.ajax({
        url: "index.php",
        type: 'post',
        data: {
          "c" : "programas",
          "m": "CargarGrupo",
          "p" : id
        },
        success: function(res) { 
          var respuesta = JSON.parse(res);
          if(respuesta["Grupos"].length>0){
            $("#BodyMatePo").empty();
            $("#select_grupo").empty();
            for(var i =0; i< respuesta["Grupos"].length;i++){
              $("#select_grupo").append("<option value='"+respuesta[["Grupos"]][i]['GrupoId']+"'>Codigo: "+respuesta[["Grupos"]][i]["GrupoCodigo"]+"  ----  Nombre: "+respuesta[["Grupos"]][i]["GrupoNombre"]+"  ----  Docente: "+respuesta[["Grupos"]][i]["UsuariosNombre"] + " " + respuesta[["Grupos"]][i]["UsuariosApellido"] +"</option>"); 
            }
            for (var j =0; j< respuesta["GrupoPro"].length;j++) {
            $("#BodyMatePo").append("<tr><td>"+respuesta["GrupoPro"][j]["GrupoMateriaId"]+"</td><td>"+respuesta["GrupoPro"][j]["GrupoNombre"]+"</td><td><button onclick='elimiMatePro("+respuesta["GrupoPro"][j]["GrupoMateriaId"]+")' class='btn btn-danger'><span><i class='fas fa-trash'></i></span></button></td></tr>")
            }
          }else{
            alertify.error("¡No existen grupos registradas!");
          } 
        }
      });
  }

  //Agrega Materias al programa
  function agregarGrupoPro() {
    var grup = $("#select_grupo").val();
    var mate = $("#programaId").val();
    var dat = [grup, mate];
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "AgregaarGrupoPro",
        "p" : dat
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if (respuesta) {
          cargarGrupos(mate);
          alertify.success("Grupo agregada con exito!!");
        }else{
          if (respuesta==false){
            alertify.error("El grupo ya esta asignada a esta materia!!");
          }else{
            alertify.error(respuesta);
          }
        }
      }
    });

  }

  //Elimina materias del programa
  function elimiMatePro(id) {
    var progra = $("#programaId").val();
    $.ajax({
      url: "index.php",
      type: 'post',
      data: {
        "c" : "programas",
        "m": "EliminarGrupoPro",
        "p" : id
      },
      success: function(res) { 
        var respuesta = JSON.parse(res);
        if (respuesta) {
          //AgregarMate(progra);
          cargarGrupos(progra);
          alertify.success("Grupo eliminada con exito!!");
        }else{
          alertify.error(respuesta);
        }
      }
    });
  }
</script>
     