<div class="container-fluid" id="Contenedor">
    <!-- Seleccion de periodo -->
    <div class="text-center">
        <h1 class="h3 mb-0 text-gray-800">Oferta de Programas</h1> 
    </div>
    <div class="col-xl-6 mb-4">
        <div class="row">
            <label for="periodo">Periodo</label>
            <select name="periodo" id="selectPeriodo" class="form-select col-md-10">
                <option selected>Select</option>
            </select>
            <button onclick="CargarProgramas()" class="btn btn-primary col-md-1 ml-1"><span class="fa fa-search"></span></button>
       </div>
    </div>

    <div class="row" id="divProgram">
        <h1 class="h3 text-center mt-5 text-gray-800">Seleccione el periodo</h1>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var url2 = "index.php";
        cargar();
    });

    //Funcion para cargar el combo de periodo
    function cargar(){
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "programas",
            "m": "CargarPeriodo",
            "p" : 0
          },
          success: function(res) { 
            var prueba = JSON.parse(res);
            for(var i =0; i< prueba.length;i++){
               $("#selectPeriodo").append("<option value='"+prueba[i]['PeriodoId']+"'>"+prueba[i]["NombrePeriodo"]+"</option>"); 
               
            }

            
          }
        });
    }

    //Funcion para obtener los programas de ese periodo
    function CargarProgramas() {
        var peri = $("#selectPeriodo").val();
        $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "programas",
            "m": "CargarProgramas",
            "p" : peri
          },
          success: function(res) { 
            var programas = JSON.parse(res);

            $("#divProgram").empty();
            for(var i =0; i< programas.length;i++){
                var color = "green";
                if(programas[i]["EstadoNombre"] == "Inactivo"){
                    color = "red";
                }

                $("#divProgram").append('<div onclick="cargarOfertaM('+programas[i]["ProgramasId"]+')" class="col-xl-6 text-start btn col-md-6 mb-0"><div class="card shadow mb-4 border-left-primary"><div class="card-header py-3">                 <h6 class="m-0 font-weight-bold text-primary">'+programas[i]["ProgramasNombre"]+'</h6></div><div class="card-body">          <strong>Descripcción: </strong>'+programas[i]["ProgramasDescripcion"]+'</br><strong>Facultad: </strong>'+programas[i]["FacultadNombre"]+'</br><strong>Tipo: </strong>'+programas[i]["TipoProgramaNombre"]+'</br><strong>Estado: </strong><label style="color:'+color+'">'+programas[i]["EstadoNombre"]+'</label></div></div></div>');
            }

          }
        });
    }

    function AbrirModal() {
      $('#exampleModalCenterUno').modal('show');
    }

    $(".closesMa").click(function () {
      $('#exampleModalCenterUno').modal('hide');
    });

    function cargarOfertaM(id) {
      $.ajax({
          url: "index.php",
          type: 'post',
          data: {
            "c" : "programas",
            "m": "CargarDatPro",
            "p" : id
          },
          success: function(res) { 
            var resp = JSON.parse(res);

            $("#Contenedor").empty();
            $("#Contenedor").append("<div class='row'><button class='btn col-md-1' onclick='volverProgramas()' id='atras'><span><i class='fs-1 fas fa-chevron-circle-left'></i></span></button><h4 class='col-md-11 text-center text-primary'><strong>"+resp["Programa"][0]["ProgramasNombre"]+"</strong></h4></div>");
            $("#Contenedor").append('<table class="table table-hover">'+
                '<thead><tr>'+
                  '<th scope="col">Codigo</th>'+
                  '<th scope="col">Nombre</th>'+
                  '<th scope="col">Semestre</th>'+
                  '<th scope="col">Grupos</th>'+
                  '<th scope="col">Agregar Grupo</th>'+
                '</tr></thead>'+
                '<tbody id="tbodypm"></tbody>'+
              '</table>');
            for (var i = 0; i < resp["Materias"].length; i++) {
              for (var e = 0; e < resp["Programa"].length; e++) {
                $("#tbodypm").append('<tr><td>'+resp["Materias"][i]["MateriasCodigo"]+'</td><td>'+resp["Materias"][i]["MateriasNombre"]+'</td><td>'+resp["Materias"][i]["MateriasSemestre"]+'</td><td>'+resp["Materias"][i]["coun"]+'</td><td><button id="'+resp["Programa"][e]["ProgramasId"]+'" onclick="Pruebas2('+resp["Materias"][i]["MateriasId"]+')" class="btn btn-warning">Agregar Grupo</button></td></tr>')
              }    
            }

          }
      });
    }

    function Pruebas2(id) {
      redirect('Programa/ProgramasMaterias.php?proId='+id);
    }

    function volverProgramas() {
      redirect('Programa/viewProgramas.php');
    }


</script>