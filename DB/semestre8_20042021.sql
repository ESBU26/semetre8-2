-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-04-2021 a las 08:04:42
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semestre8`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EstadoId` int(15) NOT NULL,
  `EstadoNombre` varchar(50) NOT NULL,
  `EstadoAfectacion` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `EstadoNombre`, `EstadoAfectacion`) VALUES
(1, 'Activo', 0),
(2, 'Inactivo', 0),
(3, 'Activo', 80),
(4, 'Inactivo', 80),
(5, 'Activo', 60),
(6, 'Inactivo', 60),
(7, 'Activo', 20),
(8, 'Inactivo', 20),
(9, 'Activo', 40),
(10, 'Inactivo', 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `FacultadId` int(15) NOT NULL,
  `FacultadNombre` varchar(150) NOT NULL,
  `FacultadDescripcion` varchar(250) NOT NULL,
  `FacultadSedeId` int(15) NOT NULL,
  `EstadoId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`FacultadId`, `FacultadNombre`, `FacultadDescripcion`, `FacultadSedeId`, `EstadoId`) VALUES
(2, 'Ingenieria', 'Facultad Tecnologica', 2, 1),
(3, 'Administrativa', 'Facultad de Administracion', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultadsede`
--

CREATE TABLE `facultadsede` (
  `FacultadSedeId` int(15) NOT NULL,
  `FacultadSedeNombre` varchar(150) NOT NULL,
  `FacultadSedeDireccion` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `facultadsede`
--

INSERT INTO `facultadsede` (`FacultadSedeId`, `FacultadSedeNombre`, `FacultadSedeDireccion`) VALUES
(1, 'Sede Artes', '12'),
(2, 'Sede tecnologica', '21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `GrupoId` int(15) NOT NULL,
  `GrupoCodigo` int(15) NOT NULL,
  `GrupoNombre` varchar(150) NOT NULL,
  `GrupoCapacidad` int(15) NOT NULL,
  `UsuariosId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`GrupoId`, `GrupoCodigo`, `GrupoNombre`, `GrupoCapacidad`, `UsuariosId`) VALUES
(1, 800, '800-NT-ProgramaciónSoftware', 25, 3),
(2, 600, '600-NT-Excel', 25, 3),
(3, 500, '500-prueba', 25, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupomateria`
--

CREATE TABLE `grupomateria` (
  `GrupoMateriaId` int(15) NOT NULL,
  `GrupoId` int(15) NOT NULL,
  `MateriaId` int(15) NOT NULL,
  `Periodo` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grupomateria`
--

INSERT INTO `grupomateria` (`GrupoMateriaId`, `GrupoId`, `MateriaId`, `Periodo`) VALUES
(2, 3, 5, 1),
(3, 1, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `MateriasId` int(15) NOT NULL,
  `MateriasCodigo` int(15) NOT NULL,
  `MateriasNombre` varchar(150) NOT NULL,
  `MateriasSemestre` int(15) NOT NULL,
  `MateriasIntencidad_H` int(15) NOT NULL,
  `MateriasDescripcion` varchar(250) NOT NULL,
  `EstadoId` int(15) NOT NULL,
  `TipoMateriaId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`MateriasId`, `MateriasCodigo`, `MateriasNombre`, `MateriasSemestre`, `MateriasIntencidad_H`, `MateriasDescripcion`, `EstadoId`, `TipoMateriaId`) VALUES
(5, 0, 'Calculo', 6, 4, 'Calculo Integral', 5, 2),
(6, 0, 'Fisica 1', 8, 6, 'Fisica mecanica', 5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `IdMenu` int(11) NOT NULL COMMENT 'Clave primaria',
  `MenusNombre` varchar(50) NOT NULL COMMENT 'Id Menu',
  `MenusUrl` varchar(100) NOT NULL COMMENT 'Nombre Menu',
  `MenusJerarquia` int(11) NOT NULL COMMENT 'Jerarquia Menu',
  `Padre` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`IdMenu`, `MenusNombre`, `MenusUrl`, `MenusJerarquia`, `Padre`) VALUES
(1, 'Administración', 'MenuView/Menu.php', 0, 0),
(2, 'Rols', 'Rols/RolsView.php', 0, 0),
(3, 'Gestión Programas', 'Programa/GestionPrograma.php', 1, 1),
(5, 'Oferta', 'Programa/viewProgramas.php', 0, 0),
(8, 'Gestión Materias', 'Materia/GestionMateria.php', 1, 1),
(9, 'Gestión Usuarios', 'Usuario/UsuariosView.php', 1, 1),
(11, 'Gestión Grupos', 'Grupos/GruposView.php', 1, 1),
(13, 'dfs', 'df', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menuxrol`
--

CREATE TABLE `menuxrol` (
  `IdReferencia` int(11) NOT NULL COMMENT 'Clave primaria',
  `IdRol` int(11) NOT NULL COMMENT 'Id Rol',
  `IdMenu` int(11) NOT NULL COMMENT 'Id Menu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `menuxrol`
--

INSERT INTO `menuxrol` (`IdReferencia`, `IdRol`, `IdMenu`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 1, 5),
(5, 1, 3),
(7, 1, 8),
(8, 1, 9),
(10, 1, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo`
--

CREATE TABLE `periodo` (
  `PeriodoId` int(11) NOT NULL,
  `NombrePeriodo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodo`
--

INSERT INTO `periodo` (`PeriodoId`, `NombrePeriodo`) VALUES
(1, '2020-2'),
(2, '2021-1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE `programas` (
  `ProgramasId` int(15) NOT NULL,
  `ProgramasNombre` varchar(150) NOT NULL,
  `ProgramasDescripcion` varchar(250) NOT NULL,
  `Periodo` int(11) NOT NULL,
  `TipoProgramaId` int(15) NOT NULL,
  `FacultadId` int(15) NOT NULL,
  `estado_EstadoId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `programas`
--

INSERT INTO `programas` (`ProgramasId`, `ProgramasNombre`, `ProgramasDescripcion`, `Periodo`, `TipoProgramaId`, `FacultadId`, `estado_EstadoId`) VALUES
(1, 'Ingenieria de Software', 'EGRESADOS - NOCTURNO', 1, 1, 2, 3),
(4, 'Pruebas', 'Pruebitas', 2, 2, 2, 3),
(5, 'Fotografia', 'Pruebas', 2, 3, 2, 3),
(11, 'Pruebas Finales', 'Ultimas pruebas', 2, 1, 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programasmaterias`
--

CREATE TABLE `programasmaterias` (
  `ProgramasMateriasId` int(15) NOT NULL,
  `ProgramasId` int(15) NOT NULL,
  `MateriaId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `programasmaterias`
--

INSERT INTO `programasmaterias` (`ProgramasMateriasId`, `ProgramasId`, `MateriaId`) VALUES
(13, 1, 5),
(14, 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `RolId` int(11) NOT NULL COMMENT 'Clave primaria',
  `RolNombre` varchar(50) NOT NULL COMMENT 'nombre',
  `RolEstado` int(11) NOT NULL COMMENT 'Estado',
  `RolFechaCreacion` date NOT NULL COMMENT 'Fecha Creacion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`RolId`, `RolNombre`, `RolEstado`, `RolFechaCreacion`) VALUES
(1, 'Administrador', 1, '2021-04-13'),
(2, 'Empleado', 1, '2021-04-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomateria`
--

CREATE TABLE `tipomateria` (
  `TipoMateriaId` int(15) NOT NULL,
  `TipoMateriaNombre` varchar(150) NOT NULL,
  `TipoMateriaDescripcion` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipomateria`
--

INSERT INTO `tipomateria` (`TipoMateriaId`, `TipoMateriaNombre`, `TipoMateriaDescripcion`) VALUES
(1, 'sdf', 'sdf'),
(2, 'Tipo Materia 1', 'Descripción 1'),
(3, 'Tipo Materia 2', 'Descripción 2'),
(4, 'Tipo Materia 3', 'Descripción 3'),
(5, 'Tipo Materia 4', 'Descripción 4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoprograma`
--

CREATE TABLE `tipoprograma` (
  `TipoProgramaId` int(15) NOT NULL,
  `TipoProgramaNombre` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoprograma`
--

INSERT INTO `tipoprograma` (`TipoProgramaId`, `TipoProgramaNombre`) VALUES
(1, 'Grado'),
(2, 'Pre-Grado'),
(3, 'Post-Grado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposvinculacion`
--

CREATE TABLE `tiposvinculacion` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiposvinculacion`
--

INSERT INTO `tiposvinculacion` (`Id`, `Nombre`) VALUES
(1, 'Administrativa'),
(2, 'Academica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuariosId` int(11) NOT NULL COMMENT 'Clave primaria',
  `UsuariosNombre` varchar(50) NOT NULL COMMENT 'nombre',
  `UsuariosApellido` varchar(100) NOT NULL COMMENT 'Apellidos',
  `UsuariosDocumentos` varchar(30) NOT NULL COMMENT 'Documento',
  `UsuariosCorreo` varchar(100) NOT NULL COMMENT 'Correo',
  `UsuariosContrasena` varchar(100) NOT NULL COMMENT 'Contrasena',
  `UsuariosTelefono` varchar(30) NOT NULL COMMENT 'Telefono',
  `roles_RolId` int(11) NOT NULL,
  `estado_EstadoId` int(15) NOT NULL,
  `tiposvinculacion_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuariosId`, `UsuariosNombre`, `UsuariosApellido`, `UsuariosDocumentos`, `UsuariosCorreo`, `UsuariosContrasena`, `UsuariosTelefono`, `roles_RolId`, `estado_EstadoId`, `tiposvinculacion_Id`) VALUES
(1, 'Edwin Santiago', 'Briceño Uribe', '12345678', 'esanti1020@gmail.com', '25d55ad283aa400af464c76d713c07ad', '5235871', 1, 1, 1),
(2, 'Oscar', 'Torres Ortega', '87654321', 'oscar@gmail.com', '25d55ad283aa400af464c76d713c07ad', '8959845', 2, 1, 2),
(3, 'luis', 'sanchez', '126458456', 'guillemo@gmail.com', '126458456', '754254', 2, 9, 2),
(4, 'Juan Camilo', 'Oscategui Rodriguez', '1000728093', 'Oscateguic@gmail.com', '1000728093', '3193146273', 1, 9, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EstadoId`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`FacultadId`),
  ADD KEY `EstadoId` (`EstadoId`),
  ADD KEY `FacultadSedeId` (`FacultadSedeId`);

--
-- Indices de la tabla `facultadsede`
--
ALTER TABLE `facultadsede`
  ADD PRIMARY KEY (`FacultadSedeId`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`GrupoId`),
  ADD KEY `UsuariosId` (`UsuariosId`);

--
-- Indices de la tabla `grupomateria`
--
ALTER TABLE `grupomateria`
  ADD PRIMARY KEY (`GrupoMateriaId`),
  ADD KEY `GrupoId` (`GrupoId`),
  ADD KEY `MateriaId` (`MateriaId`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`MateriasId`),
  ADD KEY `TipoMateriaId` (`TipoMateriaId`),
  ADD KEY `EstadosId` (`EstadoId`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`IdMenu`),
  ADD UNIQUE KEY `MenusNombre` (`MenusNombre`);

--
-- Indices de la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  ADD PRIMARY KEY (`IdReferencia`),
  ADD KEY `IdRol` (`IdRol`),
  ADD KEY `IdMenu` (`IdMenu`);

--
-- Indices de la tabla `periodo`
--
ALTER TABLE `periodo`
  ADD PRIMARY KEY (`PeriodoId`);

--
-- Indices de la tabla `programas`
--
ALTER TABLE `programas`
  ADD PRIMARY KEY (`ProgramasId`),
  ADD KEY `FacultadId` (`FacultadId`),
  ADD KEY `TipoProgramaId` (`TipoProgramaId`),
  ADD KEY `fk_programas_estado1_idx` (`estado_EstadoId`),
  ADD KEY `Periodo` (`Periodo`);

--
-- Indices de la tabla `programasmaterias`
--
ALTER TABLE `programasmaterias`
  ADD PRIMARY KEY (`ProgramasMateriasId`),
  ADD KEY `ProgramasId` (`MateriaId`),
  ADD KEY `MateriasId` (`ProgramasId`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RolId`),
  ADD UNIQUE KEY `UsuariosCorreo` (`RolId`);

--
-- Indices de la tabla `tipomateria`
--
ALTER TABLE `tipomateria`
  ADD PRIMARY KEY (`TipoMateriaId`);

--
-- Indices de la tabla `tipoprograma`
--
ALTER TABLE `tipoprograma`
  ADD PRIMARY KEY (`TipoProgramaId`);

--
-- Indices de la tabla `tiposvinculacion`
--
ALTER TABLE `tiposvinculacion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuariosId`),
  ADD UNIQUE KEY `UsuariosCorreo` (`UsuariosCorreo`),
  ADD KEY `fk_usuarios_roles1_idx` (`roles_RolId`),
  ADD KEY `fk_usuarios_estado1_idx` (`estado_EstadoId`),
  ADD KEY `fk_usuarios_tiposvinculacion1_idx` (`tiposvinculacion_Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `EstadoId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `FacultadId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `facultadsede`
--
ALTER TABLE `facultadsede`
  MODIFY `FacultadSedeId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `GrupoId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `grupomateria`
--
ALTER TABLE `grupomateria`
  MODIFY `GrupoMateriaId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `MateriasId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `IdMenu` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  MODIFY `IdReferencia` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `periodo`
--
ALTER TABLE `periodo`
  MODIFY `PeriodoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `programas`
--
ALTER TABLE `programas`
  MODIFY `ProgramasId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `programasmaterias`
--
ALTER TABLE `programasmaterias`
  MODIFY `ProgramasMateriasId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `RolId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipomateria`
--
ALTER TABLE `tipomateria`
  MODIFY `TipoMateriaId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipoprograma`
--
ALTER TABLE `tipoprograma`
  MODIFY `TipoProgramaId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tiposvinculacion`
--
ALTER TABLE `tiposvinculacion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuariosId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD CONSTRAINT `EstadoId` FOREIGN KEY (`EstadoId`) REFERENCES `estado` (`EstadoId`),
  ADD CONSTRAINT `FacultadSedeId` FOREIGN KEY (`FacultadSedeId`) REFERENCES `facultadsede` (`FacultadSedeId`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `UsuariosId` FOREIGN KEY (`UsuariosId`) REFERENCES `usuarios` (`UsuariosId`);

--
-- Filtros para la tabla `grupomateria`
--
ALTER TABLE `grupomateria`
  ADD CONSTRAINT `GrupoId` FOREIGN KEY (`GrupoId`) REFERENCES `grupo` (`GrupoId`),
  ADD CONSTRAINT `MateriaId` FOREIGN KEY (`MateriaId`) REFERENCES `materias` (`MateriasId`);

--
-- Filtros para la tabla `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `EstadosId` FOREIGN KEY (`EstadoId`) REFERENCES `estado` (`EstadoId`),
  ADD CONSTRAINT `TipoMateriaId` FOREIGN KEY (`TipoMateriaId`) REFERENCES `tipomateria` (`TipoMateriaId`);

--
-- Filtros para la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  ADD CONSTRAINT `menuxrol_ibfk_1` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`RolId`),
  ADD CONSTRAINT `menuxrol_ibfk_2` FOREIGN KEY (`IdMenu`) REFERENCES `menus` (`IdMenu`);

--
-- Filtros para la tabla `programas`
--
ALTER TABLE `programas`
  ADD CONSTRAINT `FacultadId` FOREIGN KEY (`FacultadId`) REFERENCES `facultad` (`FacultadId`),
  ADD CONSTRAINT `Periodo` FOREIGN KEY (`Periodo`) REFERENCES `periodo` (`PeriodoId`),
  ADD CONSTRAINT `TipoProgramaId` FOREIGN KEY (`TipoProgramaId`) REFERENCES `tipoprograma` (`TipoProgramaId`),
  ADD CONSTRAINT `fk_programas_estado1` FOREIGN KEY (`estado_EstadoId`) REFERENCES `estado` (`EstadoId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `programasmaterias`
--
ALTER TABLE `programasmaterias`
  ADD CONSTRAINT `MateriasId` FOREIGN KEY (`ProgramasId`) REFERENCES `programas` (`ProgramasId`),
  ADD CONSTRAINT `ProgramasId` FOREIGN KEY (`MateriaId`) REFERENCES `materias` (`MateriasId`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_estado1` FOREIGN KEY (`estado_EstadoId`) REFERENCES `estado` (`EstadoId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_roles1` FOREIGN KEY (`roles_RolId`) REFERENCES `roles` (`RolId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_tiposvinculacion1` FOREIGN KEY (`tiposvinculacion_Id`) REFERENCES `tiposvinculacion` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
