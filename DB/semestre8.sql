-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-03-2021 a las 00:42:15
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semestre8`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `IdMenu` int(11) NOT NULL COMMENT 'Clave primaria',
  `MenusNombre` varchar(50) NOT NULL COMMENT 'Id Menu',
  `MenusUrl` varchar(100) NOT NULL COMMENT 'Nombre Menu',
  `MenusJerarquia` int(11) NOT NULL COMMENT 'Jerarquia Menu',
  `Padre` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`IdMenu`, `MenusNombre`, `MenusUrl`, `MenusJerarquia`, `Padre`) VALUES
(1, 'Menus', 'index.php?c=menu&m=index', 0, NULL),
(2, 'Rols', 'index.php?c=rols&m=index', 0, NULL),
(3, 'Gestion Rol Menu', 'index.php?c=rols&m=rolMenu', 1, 1),
(37, 'Pruebas Hijos', 'Pruebas', 1, 1),
(38, 'Rolhijo', 'dfsdf', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menuxrol`
--

CREATE TABLE `menuxrol` (
  `IdReferencia` int(11) NOT NULL COMMENT 'Clave primaria',
  `IdRol` int(11) NOT NULL COMMENT 'Id Rol',
  `IdMenu` int(11) NOT NULL COMMENT 'Id Menu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `menuxrol`
--

INSERT INTO `menuxrol` (`IdReferencia`, `IdRol`, `IdMenu`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 2),
(5, 2, 3),
(6, 1, 37),
(7, 1, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `RolId` int(11) NOT NULL COMMENT 'Clave primaria',
  `RolNombre` varchar(50) NOT NULL COMMENT 'nombre',
  `RolEstado` int(11) NOT NULL COMMENT 'Estado',
  `RolFechaCreacion` date NOT NULL COMMENT 'Fecha Creacion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`RolId`, `RolNombre`, `RolEstado`, `RolFechaCreacion`) VALUES
(1, 'Administrador', 1, '2021-02-27'),
(2, 'Empleado', 1, '2021-02-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuariosId` int(11) NOT NULL COMMENT 'Clave primaria',
  `UsuariosNombre` varchar(50) NOT NULL COMMENT 'nombre',
  `UsuariosApellido` varchar(100) NOT NULL COMMENT 'Apellidos',
  `UsuariosDocumentos` varchar(30) NOT NULL COMMENT 'Documento',
  `UsuariosCorreo` varchar(100) NOT NULL COMMENT 'Correo',
  `UsuariosContrasena` varchar(100) NOT NULL COMMENT 'Contrasena',
  `UsuariosTelefono` varchar(30) NOT NULL COMMENT 'Telefono'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuariosId`, `UsuariosNombre`, `UsuariosApellido`, `UsuariosDocumentos`, `UsuariosCorreo`, `UsuariosContrasena`, `UsuariosTelefono`) VALUES
(1, 'Edwin Santiago', 'Briceño Uribe', '12345678', 'esanti1020@gmail.com', '25d55ad283aa400af464c76d713c07ad', '5235871'),
(2, 'Oscar', 'Torres Ortega', '87654321', 'oscar@gmail.com', '25d55ad283aa400af464c76d713c07ad', '8959845');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioxrol`
--

CREATE TABLE `usuarioxrol` (
  `IdReferencia` int(11) NOT NULL COMMENT 'Clave primaria',
  `IdUsuario` int(11) NOT NULL COMMENT 'Id Usuario',
  `IdRol` int(11) NOT NULL COMMENT 'Id Usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarioxrol`
--

INSERT INTO `usuarioxrol` (`IdReferencia`, `IdUsuario`, `IdRol`) VALUES
(1, 1, 1),
(2, 2, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`IdMenu`),
  ADD UNIQUE KEY `MenusNombre` (`MenusNombre`);

--
-- Indices de la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  ADD PRIMARY KEY (`IdReferencia`),
  ADD KEY `IdRol` (`IdRol`),
  ADD KEY `IdMenu` (`IdMenu`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RolId`),
  ADD UNIQUE KEY `UsuariosCorreo` (`RolId`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuariosId`),
  ADD UNIQUE KEY `UsuariosCorreo` (`UsuariosCorreo`);

--
-- Indices de la tabla `usuarioxrol`
--
ALTER TABLE `usuarioxrol`
  ADD PRIMARY KEY (`IdReferencia`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdRol` (`IdRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `IdMenu` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  MODIFY `IdReferencia` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `RolId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuariosId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarioxrol`
--
ALTER TABLE `usuarioxrol`
  MODIFY `IdReferencia` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `menuxrol`
--
ALTER TABLE `menuxrol`
  ADD CONSTRAINT `menuxrol_ibfk_1` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`RolId`),
  ADD CONSTRAINT `menuxrol_ibfk_2` FOREIGN KEY (`IdMenu`) REFERENCES `menus` (`IdMenu`);

--
-- Filtros para la tabla `usuarioxrol`
--
ALTER TABLE `usuarioxrol`
  ADD CONSTRAINT `usuarioxrol_ibfk_1` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`UsuariosId`),
  ADD CONSTRAINT `usuarioxrol_ibfk_2` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`RolId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
