<?php 
class HomeController extends IndexController{
    
	private $modelUser;

	public function __construct(){
		$this->modelUser = $this->model('user');
	}
	
	public function index(){
		$r='';
		$this->view('login','login',$r);
	}
    
	public function login(){
		
		$r = $this->modelUser->login($_POST);
		$_SESSION['usuarioId'] = $r->UsuariosId;
		$_SESSION['UsuariosNombre'] = $r->UsuariosNombre . " " . $r->UsuariosApellido;
		$sesi = $_SESSION['usuarioId'];
		if ($sesi != null || $sesi != "") {
			header("Location: index.php?c=client&m=index");
		}else {
			header("Location: index.php?c=home&m=index");
		}
		
	}
	public function closeSesion(){
		session_destroy();
		header('Location: index.php?c=home&m=index');
	} 
}
 ?>