<?php 
class GruposController extends IndexController{
    
	private $modelGrupo;

	public function __construct(){
        $this->modelGrupo = $this->model('Grupos');
		$this->modelClient = $this->model('client');
        $this->modelUser = $this->model('user');
	}

	public function index(){
    	$r = $this->modelClient->consultarMenu($_SESSION['usuarioId']);
        $datos = $this->modelUser->datosUsuario($_SESSION['usuarioId']);
        $GruposLista = $this->modelGrupo->listar();
        $this->view('inc','menu',$r,$datos);
        $this->view('Grupos','GruposView', $GruposLista,$datos);
	}

	public function listar()
	{
		$Grupolist = $this->modelGrupo->listar();
		$tabla = '';

		foreach ($Grupolist as $listado) {
            
			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->GrupoId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->GrupoId.')\"><i class=\"fas fa-trash\"></i></button>';
			
			$tabla .= '{
						"GrupoId":"'.$listado->GrupoId.'",
						"GrupoCodigo":"'.$listado->GrupoCodigo.'",
						"GrupoNombre":"'.$listado->GrupoNombre.'",
						"GrupoCapacidad":"'.$listado->GrupoCapacidad.'",
						"UsuariosDocumentos":"'.$listado->UsuariosDocumentos.'",
						"UsuariosNombre":"'.$listado->UsuariosNombre.'",
						"UsuariosApellido":"'.$listado->UsuariosApellido.'",
						"acciones":"<div class=\"btn-group\">'.$editar.$eliminar.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);

		echo '{"data":['.$tabla.']}';
	}
	
	public function createGrupo()
	{
		$r = $this->modelGrupo->createGrupo($_POST);
		if($r){
			$r = "Dato guardado con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;
	}

	public function deleteGrupo()
	{
		$r = $this->modelGrupo->deleteGrupo($_POST);

		if($r){
			$r = "Dato eliminado con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;	
	}

	public function ConsultaGrupo()
	{
		$regis = $this->modelGrupo->selectUno($_POST);

		foreach ($regis as $listado) {
			$editar = array(
				'GrupoId' => $listado->GrupoId,
				'GrupoCodigo' => $listado->GrupoCodigo,
				'GrupoNombre' => $listado->GrupoNombre,
				'GrupoCapacidad' => $listado->GrupoCapacidad,
				'UsuariosId' => $listado->UsuariosId
			);
		}
		echo json_encode($editar);
	}

	public function editGrupo()
	{
		$r = $this->modelGrupo->editGrupo($_POST);
		if($r){
			$r = "Datos actualizados con exito!!!";
		}else{
			$r = 0;
		}
		
		echo $r;	
	}

	public function CargarUsuarios()
	{
		$lista = $this->modelGrupo->CargarUsuarios();
		echo json_encode($lista);	
	}

	public function listarCombos()
	{
		$listaUsuarios = $this->modelGrupo->CargarUsuarios();

		$listadosA = array('Usuarios' => $listaUsuarios);

		echo json_encode($listadosA);
	}
}
?>