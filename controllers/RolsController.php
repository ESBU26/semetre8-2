<?php 
class RolsController extends IndexController{
    
	private $modelRols;

	public function __construct(){
		$this->modelRols = $this->model('Rols');
		$this->modelClient = $this->model('client');
        $this->modelUser = $this->model('user');
	}

	public function index(){
    	$r = $this->modelClient->consultarMenu($_SESSION['usuarioId']);
        $datos = $this->modelUser->datosUsuario($_SESSION['usuarioId']);
        $rolLista = $this->modelRols->listar();
        $this->view('inc','menu',$r,$datos);
        $this->view('Rols','RolsView', $rolLista,$datos);
	}
	
	public function listar()
	{
		$rolLista = $this->modelRols->listar();
		$tabla = '';


		foreach ($rolLista as $listado) {
			if ($listado->RolEstado== 0) {
				$estado = "Inactivo";				
			}else{
				$estado = "Activo";
				/*foreach ($menulist as $menuspadre) {
					if($menuspadre->IdMenu == $listado->Padre){
						$jerar = 	"SubMenu";
						$estado = $menuspadre->MenusNombre;
					}
				}	*/	
			}

			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->RolId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->RolId.')\"><i class=\"fas fa-trash\"></i></button>';
			$GestionMenus = '<button class=\"btn btn-info\" onclick=\"VisualizarGestion('.$listado->RolId.')\"><i class=\"fas fa-align-justify\"></i></button>';
			
			$tabla .= '{
						"RolId":"'.$listado->RolId.'",
						"RolNombre":"'.$listado->RolNombre.'",
						"RolEstado":"'.$estado.'",
						"acciones":"<div class=\"btn-group\">'.$editar.$eliminar.$GestionMenus.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);

		echo '{"data":['.$tabla.']}';
	}

    public function createRol()
	{
		$r = $this->modelRols->createRol($_POST);
		if($r){
			$r = "Datos Guardados con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;
	}

	public function createSubMenu()
	{
		echo ($_POST["p"][2]);
		if($_POST["p"][2] == "Seleccionado"){
			$r = $this->modelRols->createSubMenu($_POST);
		}else{
			echo "llave " . $_POST["p"][0];
			$r = $this->modelRols->eliminarSubMenu($_POST["p"][0]);
		}

		if($r){
			$r = "Datos Guardados con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;
	}

	public function deleteRol()
	{
		$r = $this->modelRols->deleteRol($_POST);

		if($r){
			$r = "Datos Eliminados con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;	
	}

	public function ConsultaRol()
	{
		$regis = $this->modelRols->selectUno($_POST);

		foreach ($regis as $listado) {
			$editar = array(
				'RolId' => $listado->RolId,
				'RolNombre' => $listado->RolNombre,
				'RolEstado' => $listado->RolEstado);
		}
		echo json_encode($editar);
	}

	public function editRol()
	{
		$r = $this->modelRols->editRol($_POST);
		if($r){
			$r = "Datos Modificados con exito!!!";
		}else{
			$r = 0;
		}
		
		echo $r;	
	}

}

?>
