<?php  
class AdminController extends IndexController{
    private $modelAdmi;

    //constructor donde llamamos el modelo
    public function __construct(){
        $this->modelAdmi = $this->model('admin');
    }

    //funcion que llama la vista
    public function index(){
        $r='';
        $this->view('inc','menu');  
        $this->view('admi','index',$r);
    }
    
    //funcion consultar peliculas activadas registradas
    public function consultMovieActi(){
        $r = $this->modelAdmi->consultMovieActi();
        include_once 'views/inc/header.php';
        include_once 'views/inc/menu.php';
        include_once 'views/admi/peliculas.php';
        include_once 'views/inc/footer.php';   
    }

    //funcion que llama la vista
    public function viewCreateMovie(){
        $r='';
        $this->view('inc','menu');  
        $this->view('admi','crearPeliculas',$r);
    }
    
    //funcion crear peliculas
    public function createMovie(){
        $this->modelAdmi->createMovie($_POST);
        header ("Location: index.php?c=admin&m=consultMovieActi");
    }

    //vista de crear clientes
    public function viewSeller(){
        $r='';
        $this->view('inc','menu');        
        $this->view('admi','crearClientes',$r);
    }

    //funcion crear vendedores
    public function createSeller(){
        $this->modelAdmi->createSeller($_POST);
        header ("Location: index.php?c=admin&m=viewSeller");
    }

    //vista de lista de funciones
    public function viewListFunction(){
        $r = $this->modelAdmi->consultFunctions();
        include_once 'views/inc/header.php';
        include_once 'views/inc/menu.php';
        include_once 'views/admi/listaFunciones.php';
        include_once 'views/inc/footer.php';   
    }

    //vista de crear vista funciones
    public function viewFunction(){
        $r='';
        $this->view('inc','menu');        
        $this->view('admi','funciones',$r);
    }

    //funcion crear funciones
    public function createFunction(){
        $this->modelAdmi->createFunction($_POST);
        header ("Location: index.php?c=admin&m=viewListFunction");
    }
    

}
?>