<?php
/**
 * 
 */
class ProgramasController extends IndexController
{
	
	private $modelProgram;

	public function __construct(){
		$this->modelProgram = $this->model('program');
	}


	public function CargarPeriodo()
	{
		$lista = $this->modelProgram->cargarPeriodo();
		echo json_encode($lista);
		
	}

	public function CargarProgramas()
	{
		$r = $this->modelProgram->consultarProgram($_POST);
		echo json_encode($r);

	}

	public function listar()
	{
		$r = $this->modelProgram->consultarProgram($_POST);
		$tabla = '';

		foreach ($r as $listado) {
			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->ProgramasId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->ProgramasId.')\"><i class=\"fas fa-trash\"></i></button>';

			$add = '<button class=\"btn btn-success\" onclick=\"AgregarMate('.$listado->ProgramasId.')\"><i class=\"far fa-address-book\"></i></button>';



			$tabla .= '{
						"ProgramasId" :"'.$listado->ProgramasId.'",
						"ProgramasNombre" :"'.$listado->ProgramasNombre.'",
						"ProgramasDescripcion" :"'.$listado->ProgramasDescripcion.'",
						"FacultadNombre" :"'.$listado->FacultadNombre.'",
						"TipoProgramaNombre" :"'.$listado->TipoProgramaNombre.'",
						"EstadoNombre" :"'.$listado->EstadoNombre.'",
						"acciones" :"<div class=\"btn-group\">'.$editar.$eliminar.$add.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);
		

		echo '{"data":['.$tabla.']}';
	}

	/*function listarMateriasPrograma(){
		$r = $this->modelProgram->consultarProgram($_POST);
		$tabla = '';

		foreach ($r as $listado) {
			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->ProgramasId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->ProgramasId.')\"><i class=\"fas fa-trash\"></i></button>';

			$add = '<button class=\"btn btn-success\" onclick=\"AgregarMate('.$listado->ProgramasId.')\"><i class=\"far fa-address-book\"></i></button>';



			$tabla .= '{
						"ProgramasId" :"'.$listado->ProgramasId.'",
						"ProgramasNombre" :"'.$listado->ProgramasNombre.'",
						"ProgramasDescripcion" :"'.$listado->ProgramasDescripcion.'",
						"FacultadNombre" :"'.$listado->FacultadNombre.'",
						"TipoProgramaNombre" :"'.$listado->TipoProgramaNombre.'",
						"EstadoNombre" :"'.$listado->EstadoNombre.'",
						"acciones" :"<div class=\"btn-group\">'.$editar.$eliminar.$add.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);
		

		echo '{"data":['.$tabla.']}';
	}*/

	public function listarCombos()
	{
		$listaEstado = $this->modelProgram->CargarEstados();
		$listaFacultad = $this->modelProgram->CargarFacultad();
		$listaPeriodo = $this->modelProgram->cargarPeriodo();
		$listaTipo = $this->modelProgram->cargarTipoP();

		$listadosA = array('Estados' => $listaEstado,
							'Facultad' => $listaFacultad,
							'Periodo' => $listaPeriodo,
							'Tipo' => $listaTipo );

		echo json_encode($listadosA);
	}

	public function CargarMaterias()
	{
		$listadoMate = $this->modelProgram->CargarMateria();
		$matepro = $this->modelProgram->MatePro($_POST);

		$listadosB = array('Materias' => $listadoMate,
							'MatePro' => $matepro);

		echo json_encode($listadosB);
	}
	public function CargarGrupo()
	{
		$listadoMate = $this->modelProgram->CargarGrupo();
		$grupopro = $this->modelProgram->CargarGrupoM($_POST);

		$listadosB = array('Grupos' => $listadoMate,
							'GrupoPro' => $grupopro);

		echo json_encode($listadosB);
	}

	public function CrearNew()
	{
		$r = $this->modelProgram->CrearNew($_POST);
		echo json_encode($r);
	}

	public function BuscarPro()
	{
		$r = $this->modelProgram->FindOne($_POST);
		echo json_encode($r);
	}

	public function GuardarCambios()
	{
		$r = $this->modelProgram->GuardarCambios($_POST);
		echo json_encode($r);
	}
	
	public function Eliminar()
	{
		$r = $this->modelProgram->Eliminar($_POST);
		echo json_encode($r);
	}

	public function AgregaarMatePro()
	{
		$r = $this->modelProgram->AgreMatePro($_POST);
		echo json_encode($r);
	}

	public function AgregaarGrupoPro()
	{
        $idGrupo = $_POST["p"][0];
		$idMateria = $_POST["p"][1];
		$c = $this->modelProgram->consultarGrupoPro($idGrupo, $idMateria);
		if(empty($c)){
			$r = $this->modelProgram->AgreGrupoPro($_POST);
			echo json_encode($r);
		}else{
			echo json_encode(false);
		}
	}

	public function EliminarMatePro()
	{
		$r = $this->modelProgram->EliMatePro($_POST);
		echo json_encode($r);
	}

	public function EliminarGrupoPro()
	{
		$r = $this->modelProgram->EliGrupoPro($_POST);
		echo json_encode($r);
	}

	public function CargarDatPro()
	{
		$progra = $this->modelProgram->FindOne($_POST);
		$listadoMate = $this->modelProgram->MatePrograma($_POST);

		$listadosC = array('Materias' => $listadoMate,
							'Programa' => $progra);

		echo json_encode($listadosC);
	}

}

?>