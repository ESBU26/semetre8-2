<?php 
class MenuController extends IndexController{
    
	private $modelMenu;

	public function __construct(){
		$this->modelMenu = $this->model('Menu');
		$this->modelClient = $this->model('client');
        $this->modelUser = $this->model('user');
	}


	public function listar()
	{
		$menulist = $this->modelMenu->listar();
		$tabla = '';


		foreach ($menulist as $listado) {
			if ($listado->MenusJerarquia== 0) {
				$jerar = 	"Menu";
				$padre = "No aplica";				
			}else{
				foreach ($menulist as $menuspadre) {
					if($menuspadre->IdMenu == $listado->Padre){
						$jerar = 	"SubMenu";
						$padre = $menuspadre->MenusNombre;
					}
				}		
			}

			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->IdMenu.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->IdMenu.')\"><i class=\"fas fa-trash\"></i></button>';
			
			$tabla .= '{
						"IdMenu":"'.$listado->IdMenu.'",
						"MenusNombre":"'.$listado->MenusNombre.'",
						"MenusUrl":"'.$listado->MenusUrl.'",
						"MenusJerarquia":"'.$jerar.'",
						"Padre":"'.$padre.'",
						"acciones":"<div class=\"btn-group\">'.$editar.$eliminar.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);

		echo '{"data":['.$tabla.']}';
	}

	public function listarcombo()
	{
		$menulistar = $this->modelMenu->listarP();
		$comlis = '<option></option>';
		foreach ($menulistar as $combo) {
			$comlis .= "<option value=\"".$combo->IdMenu."\">".$combo->MenusNombre."</option>";
		}
		echo $comlis;
	}
	
	public function createMenu()
	{
		
		$r = $this->modelMenu->CreateModel($_POST);
		if($r){
			$r = "Datos Guardados con exito!!!";
		}else{
			$r = 0;
		}
		//header("Location: index.php?c=menu&m=index");
		echo $r;
	}

	public function eliminarMenu()
	{
		$r = $this->modelMenu->deleteMenu($_POST);

		if($r){
			$r = "Datos Eliminados con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;
	}

	public function ConsultaEdit()
	{
		$regis = $this->modelMenu->selectUno($_POST);

		foreach ($regis as $listado) {
			$editar = array(
				'Id' => $listado->IdMenu,
				'Nombre' => $listado->MenusNombre,
				'Url' => $listado->MenusUrl,
				'jerarquia' => $listado->MenusJerarquia,
				'padre' => $listado->Padre );
		}

		 

		echo json_encode($editar);
	}

	public function EditarMenus()
	{	

		$r = $this->modelMenu->editMenu($_POST);
		if($r){
			$r = "Datos Modificados con exito!!!";
		}else{
			$r = 0;
		}
		
		echo $r;	
	}


	public function listarRolesMenu()
	{
		$menulistarMenu = $this->modelMenu->listarP();
		$menulistarSub = $this->modelMenu->listarSub();
		$menusRol = $this->modelMenu->listarMenRol($_POST);
		$rolesTb = $this->modelMenu->listarRolesT();


		$MenusListas =array();
		$SubMenusListas =array();
		$MenusRoles = array();
		$RolesT = array();

		$total = array();
		foreach ($menulistarMenu as $listado) {
			$menuses = array(
				'Id' => $listado->IdMenu,
				'Nombre' => $listado->MenusNombre,
				'Url' => $listado->MenusUrl,
				'jerarquia' => $listado->MenusJerarquia,
				'padre' => $listado->Padre);
			array_push($MenusListas,$menuses);
		}

		foreach ($menulistarSub as $listado) {
			$submenuses = array(
				'Id' => $listado->IdMenu,
				'Nombre' => $listado->MenusNombre,
				'Url' => $listado->MenusUrl,
				'jerarquia' => $listado->MenusJerarquia,
				'padre' => $listado->Padre);
			array_push($SubMenusListas,$submenuses);
		}

		foreach ($menusRol as $listado) {
			$submenuses = array(
				'IdReferencia' => $listado->IdReferencia,
				'IdRol' => $listado->IdRol,
				'IdMenu' => $listado->IdMenu);
			array_push($MenusRoles,$submenuses);
		}

		foreach ($rolesTb as $listado) {
			$roleses = array(
				'RolId' => $listado->RolId,
				'RolNombre' => $listado->RolNombre,
				'RolEstado' => $listado->RolEstado);
			array_push($RolesT,$roleses);
		}
		array_push($total,$MenusListas);
		array_push($total,$SubMenusListas);
		array_push($total,$MenusRoles);
		array_push($total,$RolesT);

		echo json_encode($total);
	}
}

?>