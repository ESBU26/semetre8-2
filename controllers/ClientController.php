<?php  
class ClientController extends IndexController{
    private $modelClient;
    private $modelUser;

    //constructor donde llamamos el modelo
    public function __construct(){
        $this->modelClient = $this->model('client');
        $this->modelUser = $this->model('user');
    }

    //funcion que llama la vista
    //public function index($d){
    public function index(){
        $datos = $this->modelUser->datosUsuario($_SESSION['usuarioId']);
        $r = $this->modelClient->consultarMenu($datos->roles_RolId);
        $r2 = $this->modelClient->consultarMenuHijos($datos->roles_RolId);
        //$this->view('inc','menu', $r,$r2);
        $this->view('login','home', $r,$r2);
    }

    public function llamarMenu()
    {
        # code...
    }

 
}
?>