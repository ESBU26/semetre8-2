<?php
/**
 * 
 */
class MateriasController extends IndexController
{
	
	private $modelMateria;

	public function __construct(){
		$this->modelMateria = $this->model('materia');
	}


	public function CargarMaterias()
	{
		$lista = $this->modelMateria->cargarMaterias();
		echo json_encode($lista);
		
	}

	public function CargarProgramas()
	{
		$r = $this->modelMateria->consultarProgram($_POST);
		echo json_encode($r);

	}

	public function listar()
	{
		$r = $this->modelMateria->consultarMaterias();
		$tabla = '';

		foreach ($r as $listado) {
			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->MateriasId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->MateriasId.')\"><i class=\"fas fa-trash\"></i></button>';

			$tabla .= '{
						"MateriasId" :"'.$listado->MateriasId.'",
						"MateriasCodigo" :"'.$listado->MateriasCodigo.'",
						"MateriasNombre" :"'.$listado->MateriasNombre.'",
						"MateriasSemestre" :"'.$listado->MateriasSemestre.'",
						"MateriasIntencidad_H" :"'.$listado->MateriasIntencidad_H.'",
						"MateriasDescripcion" :"'.$listado->MateriasDescripcion.'",
						"EstadoNombre" :"'.$listado->EstadoNombre.'",
						"TipoMateriaNombre" :"'.$listado->TipoMateriaNombre.'",
						"acciones" :"<div class=\"btn-group\">'.$editar.$eliminar.'</div>"
                    },';
						//"TipoMateriaNombre" :"'.$listado->TipoMateriaNombre.'",
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);
		

		echo '{"data":['.$tabla.']}';
	}

	public function listarCombos()
	{
		$listaEstado = $this->modelMateria->CargarEstados();
		$listaMaterias = $this->modelMateria->CargarTipoM();

		$listadosA = array('Estados' => $listaEstado,
							'Materias' => $listaMaterias );

		echo json_encode($listadosA);
	}

	public function CrearNew()
	{
		$r = $this->modelMateria->CrearNew($_POST);
		echo json_encode($r);
	}

	public function BuscarPro()
	{
		$r = $this->modelMateria->FindOne($_POST);
		echo json_encode($r);
	}

	public function GuardarCambios()
	{
		$r = $this->modelMateria->GuardarCambios($_POST);
		echo json_encode($r);
	}
	
	public function Eliminar()
	{
		$r = $this->modelMateria->Eliminar($_POST);
		echo json_encode($r);
	}
}

?>