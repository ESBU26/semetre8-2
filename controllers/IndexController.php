<?php 
class IndexController{
    //buscamos las vistas
    public function view($module,$view,$r=[],$r2=[]){
        if (file_exists('views/'.$module.'/'.$view.'.php')) {
            include_once 'views/inc/header.php';
            include_once 'views/'.$module.'/'.$view.'.php';
            include_once 'views/inc/footer.php';   
        } else {
            die('No se encontro la vistas');
        }
    }
    //buscamos el modelo
    public function model($model){
        $model=ucwords($model).'Model';
        if (file_exists('models/'.$model.'.php')) {
            require_once 'models/'.$model.'.php';
            return new $model;
        }
        else{
            die('No se encontro el modelo');
        }
    }
}
?>