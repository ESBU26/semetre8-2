<?php 
class UsuarioController extends IndexController{
    
	private $modelUsuario;

	public function __construct(){
        $this->modelUsuario = $this->model('Usuarios');
		$this->modelClient = $this->model('client');
        $this->modelUser = $this->model('user');
	}

	public function index(){
    	$r = $this->modelClient->consultarMenu($_SESSION['usuarioId']);
        $datos = $this->modelUser->datosUsuario($_SESSION['usuarioId']);
        $usuariosLista = $this->modelUsuario->listar();
        $this->view('inc','menu',$r,$datos);
        $this->view('Usuario','UsuariosView', $usuariosLista,$datos);
	}

	public function listar()
	{
		$usuariolist = $this->modelUsuario->listar();
		$tabla = '';


		foreach ($usuariolist as $listado) {
            //Pendiente de validador de estado
			/*if ($listado->UsuariosId== 0) {
								
			}else{		
			}*/

			$editar = '<button class=\"btn btn-primary\" onclick=\"Modificar('.$listado->UsuariosId.')\"><i class=\"fas fa-edit\"></i></button>';	
			$eliminar = '<button class=\"btn btn-danger\" onclick=\"Eliminar('.$listado->UsuariosId.')\"><i class=\"fas fa-trash\"></i></button>';
			
			$tabla .= '{
						"UsuariosId":"'.$listado->UsuariosId.'",
						"UsuariosDocumentos":"'.$listado->UsuariosDocumentos.'",
						"UsuariosNombre":"'.$listado->UsuariosNombre.'",
						"UsuariosApellido":"'.$listado->UsuariosApellido.'",
						"UsuariosCorreo":"'.$listado->UsuariosCorreo.'",
						"UsuariosTelefono":"'.$listado->UsuariosTelefono.'",
						"RolNombre":"'.$listado->RolNombre.'",
						"Nombre":"'.$listado->Nombre.'",
						"EstadoNombre":"'.$listado->EstadoNombre.'",
						"acciones":"<div class=\"btn-group\">'.$editar.$eliminar.'</div>"
					},';
		}

		$tabla = substr($tabla,0,strlen($tabla)-1);

		echo '{"data":['.$tabla.']}';
	}
	
	public function createUsuario()
	{
		$r = $this->modelUsuario->createUsuario($_POST);
		if($r){
			$r = "Dato guardado con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;
	}

	public function deleteUsuario()
	{
		$r = $this->modelUsuario->deleteUsuario($_POST);

		if($r){
			$r = "Dato eliminado con exito!!!";
		}else{
			$r = 0;
		}
		echo $r;	
	}

	public function ConsultaUsuario()
	{
		$regis = $this->modelUsuario->selectUno($_POST);

		foreach ($regis as $listado) {
			$editar = array(
				'UsuariosId' => $listado->UsuariosId,
				'UsuariosNombre' => $listado->UsuariosNombre,
				'UsuariosApellido' => $listado->UsuariosApellido,
				'UsuariosDocumentos' => $listado->UsuariosDocumentos,
				'UsuariosCorreo' => $listado->UsuariosCorreo,
				'UsuariosTelefono' => $listado->UsuariosTelefono,
				'roles_RolId' => $listado->roles_RolId,
				'tiposvinculacion_Id' => $listado->tiposvinculacion_Id,
				'estado_EstadoId' => $listado->estado_EstadoId
			);
		}
		echo json_encode($editar);
	}

	public function editUsuario()
	{
		$r = $this->modelUsuario->editUsuario($_POST);
		if($r){
			$r = "Datos actualizados con exito!!!";
		}else{
			$r = 0;
		}
		
		echo $r;	
	}

	public function CargarEstados()
	{
		$lista = $this->modelUsuario->CargarEstados();
		echo json_encode($lista);
		
	}

	public function CargarTipoVinculacion()
	{
		$lista = $this->modelUsuario->CargarTipoVinculacion();
		echo json_encode($lista);

	}

	public function CargarRoles()
	{
		$lista = $this->modelUsuario->CargarRoles();
		echo json_encode($lista);

	}

	public function listarCombos()
	{
		$listaEstado = $this->modelUsuario->CargarEstados();
		$listaTVinculacion = $this->modelUsuario->CargarTipoVinculacion();
		$listaRoles = $this->modelUsuario->CargarRoles();

		$listadosA = array('Estados' => $listaEstado,
							'Roles' => $listaRoles,
							'TiposVinculacion' => $listaTVinculacion);

		echo json_encode($listadosA);
	}
}
?>