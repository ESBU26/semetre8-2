<?php 
//clase modelo para iniciar sesion 
class UserModel{

    private $db;
    
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function login($data){
        //sentencia iniciar sesion
        $stm = $this->db->prepare("SELECT * FROM usuarios WHERE UsuariosDocumentos = ? AND UsuariosContrasena = ?");
        $stm->execute([$data['num_ident'], md5($data['contrasenia'])]);
        return $stm->fetch(PDO::FETCH_OBJ);
    }

    public function datosUsuario($data){
        try{
            $stm = $this->db->prepare("SELECT * FROM usuarios WHERE UsuariosId = $data");
            $stm->execute();
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage()); 
        }
    }
}
?>