<?php 

class GruposModel 
{
	private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function listar()
    {
        try{
    	    $stm = $this->db->prepare("SELECT g.GrupoId, g.GrupoCodigo, g.GrupoNombre, g.GrupoCapacidad ,u.UsuariosDocumentos,u.UsuariosNombre,u.UsuariosApellido 
            FROM `grupo` as g
            INNER JOIN `usuarios` as u ON g.UsuariosId = u.UsuariosId");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function createGrupo($data)
    {
    	try{
            $stm = $this->db->prepare("INSERT INTO Grupo (GrupoCodigo,GrupoNombre,GrupoCapacidad,UsuariosId) values (?,?,?,?)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_INT);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][2], PDO::PARAM_INT);
            $stm->bindparam(4,$data["p"][3], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }

    }

    public function deleteGrupo($data)
    {
        try {
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM Grupo WHERE GrupoId = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function editGrupo($data)
    {
         try{
        $stm = $this->db->prepare("UPDATE Grupo SET GrupoCodigo = ?, GrupoNombre= ?, GrupoCapacidad=? , UsuariosId= ? WHERE GrupoId = ?");
        $stm->bindparam(1,$data["p"][1], PDO::PARAM_INT);
        $stm->bindparam(2,$data["p"][2], PDO::PARAM_STR);
        $stm->bindparam(3,$data["p"][3], PDO::PARAM_INT);
        $stm->bindparam(4,$data["p"][4], PDO::PARAM_INT);
        $stm->bindparam(5,$data["p"][0], PDO::PARAM_INT);
        $r = $stm->execute();
        return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function selectUno($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM Grupo WHERE GrupoId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function CargarUsuarios()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `usuarios`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}
 ?>
