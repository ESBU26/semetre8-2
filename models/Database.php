<?php
class DataBase{
    //funcion para conectar a la base de datos
    public static function connect(){
        try {
			$conexion=new PDO('mysql:host=localhost;dbname=semestre8;charset=utf8','root',null);
			$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conexion;
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
}
?>