<?php  
class AdminModel{
    private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    //sentencia crear clientes
    public function createSeller($data){
        try{
        $stm = $this->db->prepare("INSERT INTO usuarios (nombre,num_ident,contrasenia,roles_id_rol) values (?,?,?,2)");
        $stm->bindparam(1,$data['nombre'], PDO::PARAM_STR);
        $stm->bindparam(2,$data['num_ident'], PDO::PARAM_INT);
        $stm->bindparam(3,md5($data['contrasenia']), PDO::PARAM_STR);
        $stm->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //sentencia crear peliculas
    public function createMovie($data){
        try{
            $stm = $this->db->prepare("INSERT INTO peliculas (codigo,nombre) values (?,?)");
            $stm->bindparam(1,$data['codigo'],PDO::PARAM_INT);
            $stm->bindparam(2,$data['nombre'],PDO::PARAM_STR);
            $stm->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //consultar peliculas activas
    public function consultMovieActi(){
        try{
            $stm = $this->db->prepare("SELECT * FROM peliculas");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }       
    }

    //consultar funciones
    public function consultFunctions(){
        try{
            $stm = $this->db->prepare("SELECT * FROM `funciones`
            INNER JOIN peliculas ON funciones.peliculas_codigo = peliculas.codigo");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }       
    }

    //sentencia crear funciones
    public function createFunction($data){
        try{
        $stm = $this->db->prepare("INSERT INTO funciones (id_fun,fecha,hora,peliculas_codigo) values (?,?,?,?)");
        $stm->bindparam(1,$data['id_fun'], PDO::PARAM_INT);
        $stm->bindparam(2,$data['fecha'], PDO::PARAM_STR);
        $stm->bindparam(3,$data['hora'], PDO::PARAM_INT);
        $stm->bindparam(4,$data['peliculas_codigo'], PDO::PARAM_INT);
        $stm->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
?>