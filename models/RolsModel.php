<?php 
/**
 * 
 */
class RolsModel 
{
	private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function listar()
    {
        try{
    	    $stm = $this->db->prepare("SELECT * FROM roles");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function createRol($data)
    {
    	try{
            $fechaCreacion = new DateTime();
            $cadena_fecha_actual = $fechaCreacion->format("d/m/Y");
            $stm = $this->db->prepare("INSERT INTO roles (RolNombre,RolEstado,RolFechaCreacion) values (?,?,$cadena_fecha_actual)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }

    }

    public function createSubMenu($data)
    {
    	try{
            $stm = $this->db->prepare("INSERT INTO menuxrol (IdRol,IdMenu) values (?,?)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_INT);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }

    }

    public function eliminarSubMenu($data)
    {
        try {
            echo $data;
            $id = (int)$data["p"];
            echo "ok ".$id;
            $stm = $this->db->prepare("DELETE FROM menuxrol WHERE IdReferencia = ".$data);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function deleteRol($data)
    {
        try {
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM roles WHERE RolId = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function editRol($data)
    {
         try{
        $stm = $this->db->prepare("UPDATE roles SET RolNombre = ?, RolEstado= ? WHERE RolId = ?");
        $stm->bindparam(1,$data["p"][1], PDO::PARAM_STR);
        $stm->bindparam(2,$data["p"][2], PDO::PARAM_INT);
        $stm->bindparam(3,$data["p"][0], PDO::PARAM_INT);
        $r = $stm->execute();
        return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function selectUno($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM roles WHERE RolId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}


 ?>
