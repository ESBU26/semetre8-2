<?php 
/**
 * 
 */
class MenuModel
{
	
	 private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function listar()
    {
        try{
    	   $stm = $this->db->prepare("SELECT * FROM menus");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function listarP()
    {
        try{
           $stm = $this->db->prepare("SELECT * FROM menus WHERE MenusJerarquia = 0");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function listarSub()
    {
        try{
           $stm = $this->db->prepare("SELECT * FROM menus WHERE MenusJerarquia = 1");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function listarRolesT()
    {
        try{
           $stm = $this->db->prepare("SELECT * FROM roles");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function listarMenRol($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM `menuxrol` where IdRol = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function listarMenRol2($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM `menuxrol` where IdRol = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function CreateModel($data)
    {
    	try{
        $stm = $this->db->prepare("INSERT INTO menus (MenusNombre,MenusUrl,MenusJerarquia,Padre) values (?,?,?,?)");
        $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
        $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
        $stm->bindparam(3,$data["p"][2], PDO::PARAM_INT);
        $stm->bindparam(4,$data["p"][3], PDO::PARAM_INT);
        $r = $stm->execute();
        return $r;
        } catch (Exception $e) {
           return $r = false;
        }

    }


    public function deleteMenu($data)
    {
        try {
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM menus WHERE IdMenu = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function editMenu($data)
    {
         try{
        $stm = $this->db->prepare("UPDATE menus SET MenusNombre = ?, MenusUrl= ?, MenusJerarquia = ?, Padre = ? WHERE IdMenu = ?;");
        $stm->bindparam(1,$data["p"][1], PDO::PARAM_STR);
        $stm->bindparam(2,$data["p"][2], PDO::PARAM_STR);
        $stm->bindparam(3,$data["p"][3], PDO::PARAM_INT);
        $stm->bindparam(4,$data["p"][4], PDO::PARAM_INT);
        $stm->bindparam(5,$data["p"][0], PDO::PARAM_INT);
        $r = $stm->execute();
        return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    public function selectUno($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM menus WHERE IdMenu = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }




}




 ?>