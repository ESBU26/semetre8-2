<?php 

class UsuariosModel 
{
	private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function listar()
    {
        try{
    	    $stm = $this->db->prepare("SELECT u.UsuariosId, u.UsuariosNombre,u.UsuariosApellido,u.UsuariosDocumentos,u.UsuariosCorreo,u.UsuariosTelefono, r.RolNombre ,tv.Nombre, e.EstadoNombre 
            FROM `usuarios` as u
            INNER JOIN estado as e ON u.estado_EstadoId = e.EstadoId 
            INNER JOIN tiposvinculacion AS tv ON u.tiposvinculacion_Id = tv.Id
            INNER JOIN roles AS r ON u.roles_RolId = r.RolId");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function createUsuario($data)
    {
    	try{
            $fechaCreacion = new DateTime();
            $cadena_fecha_actual = $fechaCreacion->format("d/m/Y");
            $stm = $this->db->prepare("INSERT INTO usuarios (UsuariosNombre,UsuariosApellido,UsuariosDocumentos,UsuariosCorreo,UsuariosContrasena,UsuariosTelefono,roles_RolId ,tiposvinculacion_Id,estado_EstadoId) values (?,?,?,?,?,?,?,?,?)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][2], PDO::PARAM_STR);
            $stm->bindparam(4,$data["p"][3], PDO::PARAM_STR);
            $stm->bindparam(5,md5($data["p"][4]), PDO::PARAM_STR);
            $stm->bindparam(6,$data["p"][5], PDO::PARAM_STR);
            $stm->bindparam(7,$data["p"][6], PDO::PARAM_INT);
            $stm->bindparam(8,$data["p"][7], PDO::PARAM_INT);
            $stm->bindparam(9,$data["p"][8], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }

    }

    public function deleteUsuario($data)
    {
        try {
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM usuarios WHERE UsuariosId = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

     public function editUsuario($data)
    {
         try{
        $stm = $this->db->prepare("UPDATE usuarios SET UsuariosNombre = ?, UsuariosApellido= ?, UsuariosDocumentos = ?, UsuariosCorreo= ?, UsuariosTelefono= ?, roles_RolId= ?, tiposvinculacion_Id= ? WHERE UsuariosId = ?");
        $stm->bindparam(1,$data["p"][1], PDO::PARAM_STR);
        $stm->bindparam(2,$data["p"][2], PDO::PARAM_STR);
        $stm->bindparam(3,$data["p"][3], PDO::PARAM_STR);
        $stm->bindparam(4,$data["p"][4], PDO::PARAM_STR);
        $stm->bindparam(5,$data["p"][5], PDO::PARAM_STR);
        $stm->bindparam(6,$data["p"][6], PDO::PARAM_INT);
        $stm->bindparam(7,$data["p"][7], PDO::PARAM_INT);
        $stm->bindparam(8,$data["p"][0], PDO::PARAM_INT);
        $r = $stm->execute();
        return $r;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function selectUno($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT * FROM usuarios WHERE UsuariosId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function CargarEstados()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `estado` WHERE `EstadoAfectacion` = 40");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function CargarTipoVinculacion()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `tiposvinculacion`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function CargarRoles()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `roles`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}
 ?>
