<?php 
class MateriaModel{

    private $db;
    
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function CargarTipoM(){
        try{
            $stm = $this->db->prepare("SELECT * FROM `tipomateria`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function cargarMaterias(){
        try{
            $stm = $this->db->prepare("SELECT * FROM `materias`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function consultarMaterias()
    {
        try{
           $stm = $this->db->prepare("SELECT m.MateriasId, m.MateriasCodigo, m.MateriasNombre, m.MateriasSemestre, m.MateriasIntencidad_H, m.MateriasDescripcion, m.EstadoId, m.TipoMateriaId, tm.TipoMateriaNombre, e.EstadoId, e.EstadoNombre FROM `materias` as m INNER JOIN tipomateria as tm ON m.TipoMateriaId = tm.TipoMateriaId INNER JOIN estado as e ON m.EstadoId = e.EstadoId");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function CrearNew($data)
    {
        try{
            $stm = $this->db->prepare("INSERT INTO `materias`(`MateriasCodigo`, `MateriasNombre`, `MateriasSemestre`, `MateriasIntencidad_H`, `MateriasDescripcion`, `EstadoId`, `TipoMateriaId`) VALUES (?,?,?,?,?,?,?)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][2], PDO::PARAM_INT);
            $stm->bindparam(4,$data["p"][3], PDO::PARAM_INT);
            $stm->bindparam(5,$data["p"][4], PDO::PARAM_STR);
            $stm->bindparam(6,$data["p"][5], PDO::PARAM_INT);
            $stm->bindparam(7,$data["p"][6], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function CargarEstados()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `estado` WHERE EstadoAfectacion = 60");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function CargarFacultad()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `facultad`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

     public function cargarTipoP()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `tipoprograma`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function FindOne($data)
    {
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("SELECT * FROM `materias` WHERE MateriasId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function GuardarCambios($data)
    {
        try{
            $stm = $this->db->prepare("UPDATE `materias` SET `MateriasCodigo`=?,`MateriasNombre`=?,`MateriasSemestre`=?,`MateriasIntencidad_H`=?,`MateriasDescripcion`=?,`EstadoId`=?,`TipoMateriaId`=? WHERE MateriasId = ?");
            $stm->bindparam(1,$data["p"][0][0], PDO::PARAM_INT);
            $stm->bindparam(2,$data["p"][0][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][0][2], PDO::PARAM_INT);
            $stm->bindparam(4,$data["p"][0][3], PDO::PARAM_INT);
            $stm->bindparam(5,$data["p"][0][4], PDO::PARAM_STR);
            $stm->bindparam(6,$data["p"][0][5], PDO::PARAM_INT);
            $stm->bindparam(7,$data["p"][0][6], PDO::PARAM_INT);
            $stm->bindparam(8,$data["p"][1], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function Eliminar($data)
    {
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM `materias` WHERE `MateriasId` = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $e->getMessage();

        }
    }

}
?>