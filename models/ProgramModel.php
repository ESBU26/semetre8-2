<?php 
class ProgramModel{

    private $db;
    
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function cargarPeriodo(){
        try{
            $stm = $this->db->prepare("SELECT * FROM periodo");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function CargarGrupo(){
        try{
            $stm = $this->db->prepare("SELECT g.GrupoId, g.GrupoCodigo, g.GrupoNombre, u.UsuariosNombre, u.UsuariosApellido FROM `grupo` as g INNER JOIN usuarios as u ON g.UsuariosId = u.UsuariosId");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function CargarGrupoM($data){
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("SELECT gp.GrupoMateriaId, gp.MateriaId, g.GrupoCodigo, g.GrupoNombre, m.MateriasCodigo, m.MateriasNombre FROM `grupomateria` as gp INNER JOIN grupo as g ON g.GrupoId = gp.GrupoId INNER JOIN materias as m ON m.MateriasId = gp.MateriaId WHERE gp.MateriaId = $id");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function consultarGrupoPro($idGrupo, $idMateria){
        try{
            $stm = $this->db->prepare("SELECT * FROM `grupomateria` WHERE GrupoId = $idGrupo AND MateriaId = $idMateria");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function MatePro($data)
    {
       try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("SELECT pm.ProgramasMateriasId, mt.MateriasCodigo,mt.MateriasNombre, mt.MateriasSemestre FROM `programasmaterias` as pm INNER JOIN programas as pr ON pm.ProgramasId = pr.ProgramasId INNER JOIN materias as mt on MateriaId = mt.MateriasId WHERE pr.ProgramasId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function consultarProgram($data)
    {
        try{
            $id = (int)$data["p"];
           $stm = $this->db->prepare("SELECT p.ProgramasId, p.ProgramasNombre,p.ProgramasDescripcion, pe.NombrePeriodo,tp.TipoProgramaNombre,f.FacultadNombre, e.EstadoNombre FROM `programas` as p INNER JOIN tipoprograma as tp ON p.`TipoProgramaId`= tp.TipoProgramaId INNER JOIN facultad as f ON p.`FacultadId` = f.FacultadId INNER JOIN estado as e ON p.`estado_EstadoId` = e.EstadoId INNER JOIN Periodo AS pe ON p.Periodo = pe.PeriodoId WHERE Periodo =  ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function CrearNew($data)
    {
        try{
            $stm = $this->db->prepare("INSERT INTO `programas`(`ProgramasNombre`, `ProgramasDescripcion`, `Periodo`, `TipoProgramaId`, `FacultadId`, `estado_EstadoId`) VALUES (?,?,?,?,?,?)");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][2], PDO::PARAM_INT);
            $stm->bindparam(4,$data["p"][3], PDO::PARAM_INT);
            $stm->bindparam(5,$data["p"][4], PDO::PARAM_INT);
            $stm->bindparam(6,$data["p"][5], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function CargarEstados()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `estado` WHERE `EstadoAfectacion` = 80");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function CargarFacultad()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `facultad`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

     public function cargarTipoP()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `tipoprograma`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    
    public function CargarMateria()
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM `materias`");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

    public function FindOne($data)
    {
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("SELECT * FROM `programas` WHERE ProgramasId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function GuardarCambios($data)
    {
        try{
            $stm = $this->db->prepare("UPDATE `programas` SET `ProgramasNombre`=?,`ProgramasDescripcion`=?,`Periodo`=?,`TipoProgramaId`=?,`FacultadId`=?,`estado_EstadoId`=? WHERE ProgramasId = ?");
            $stm->bindparam(1,$data["p"][0][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][0][1], PDO::PARAM_STR);
            $stm->bindparam(3,$data["p"][0][2], PDO::PARAM_INT);
            $stm->bindparam(4,$data["p"][0][3], PDO::PARAM_INT);
            $stm->bindparam(5,$data["p"][0][4], PDO::PARAM_INT);
            $stm->bindparam(6,$data["p"][0][5], PDO::PARAM_INT);
            $stm->bindparam(7,$data["p"][1], PDO::PARAM_INT);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function Eliminar($data)
    {
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM `programas` WHERE `ProgramasId` = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $e->getMessage();

        }
    }

    public function AgreMatePro($data)
    {
        try{
            $veri = $this->Verifica($data);

            if(count($veri)==0){
                $stm = $this->db->prepare("INSERT INTO `programasmaterias`(`ProgramasId`, `MateriaId`) VALUES (?,?)");
                $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
                $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
                $r = $stm->execute();
            }else{
                $r = false;
            }
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function AgreGrupoPro($data)
    {
        try{
            $veri = $this->Verifica($data);

            //if(count($veri)==0){
                $stm = $this->db->prepare("INSERT INTO `grupomateria`(`GrupoId`, `MateriaId`) VALUES (?,?)");
                $stm->bindparam(1,$data["p"][0], PDO::PARAM_INT);
                $stm->bindparam(2,$data["p"][1], PDO::PARAM_INT);
                $r = $stm->execute();
            //}else{
            //    $r = false;
            //}
            return $r;
        } catch (Exception $e) {
            return $r = false;
        }
    }

    public function Verifica($data)
    {
        try{
            $stm = $this->db->prepare("SELECT * FROM programasmaterias WHERE ProgramasId = ? AND MateriaId = ?");
            $stm->bindparam(1,$data["p"][0], PDO::PARAM_STR);
            $stm->bindparam(2,$data["p"][1], PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function EliMatePro($data)
    {
       try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM `programasmaterias` WHERE `programasmaterias`.`ProgramasMateriasId` = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function EliGrupoPro($data)
    {
       try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("DELETE FROM `grupomateria` WHERE `GrupoMateriaId` = ".$id);
            $r = $stm->execute();
            return $r;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function MatePrograma($data)
    {
        try{
            $id = (int)$data["p"];
            $stm = $this->db->prepare("SELECT m.MateriasId, m.MateriasCodigo, m.MateriasNombre, m.MateriasSemestre, (SELECT COUNT(*) FROM `grupomateria` where MateriaId = m.MateriasId) as coun FROM `materias` as m INNER JOIN programasmaterias as pm ON m.MateriasId = pm.MateriaId WHERE pm.ProgramasId = ".$id);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
?>