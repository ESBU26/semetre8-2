<?php  
class ClientModel{
    private $db;

    //traemos la conexion
    public function __construct(){
        $this->db = DataBase::connect();
    }

    public function consultarMenu($data){
        try {
			$stm = $this->db->prepare("SELECT * FROM `menuxrol` INNER JOIN menus ON menus.IdMenu = menuxrol.IdMenu WHERE menuxrol.idRol = $data and MenusJerarquia = 0");
            $stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
    }
    
    public function consultarMenuHijos($data){
        try {
            $stm = $this->db->prepare("SELECT * FROM `menuxrol` INNER JOIN menus ON menus.IdMenu = menuxrol.IdMenu WHERE menuxrol.idRol = $data and MenusJerarquia = 1");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
?>